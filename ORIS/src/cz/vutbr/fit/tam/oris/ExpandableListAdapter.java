package cz.vutbr.fit.tam.oris;

import java.util.List;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ExpandableListAdapter extends BaseExpandableListAdapter {
	
	private Context mContext;
	private List<Event> mCalendarList;
	
	public ExpandableListAdapter(Context context, List<Event> events) {
		this.mContext = context;
		this.mCalendarList = events;
	}

	@Override
	public int getGroupCount() {
		return this.mCalendarList.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {		
		return 1;
	}

	@Override
	public Event getGroup(int groupPosition) {
		return this.mCalendarList.get(groupPosition);
	}

	@Override
	public Event getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.row, null);
		}
		
		TextView tvDate = (TextView) convertView.findViewById(R.id.row_date);
		tvDate.setText(getGroup(groupPosition).date_str);
		TextView tvName = (TextView) convertView.findViewById(R.id.row_name);
		tvName.setText(getGroup(groupPosition).name);
		TextView tvOrg = (TextView) convertView.findViewById(R.id.row_organizer);
		tvOrg.setText(getGroup(groupPosition).organizer);
		
		if (groupPosition % 2 == 1) {
			RelativeLayout rl = (RelativeLayout) convertView.findViewById(R.id.rl_row);
			rl.setBackgroundResource(R.drawable.border_grey);
		} else {
			RelativeLayout rl = (RelativeLayout) convertView.findViewById(R.id.rl_row);
			rl.setBackgroundResource(R.drawable.border);
		}
		
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_details, null);
        }
		
		// Nastaveni hodnot v podradku
		TextView region = (TextView) convertView.findViewById(R.id.row_region);
		region.setText(getGroup(groupPosition).region);
		TextView sport = (TextView) convertView.findViewById(R.id.row_sport);
		sport.setText(getGroup(groupPosition).sport_str);
		TextView discipline = (TextView) convertView.findViewById(R.id.row_discipline);
		discipline.setText(getGroup(groupPosition).discipline_str);
		TextView level = (TextView) convertView.findViewById(R.id.row_level);
		level.setText(getGroup(groupPosition).level_str);
		
		ImageView rowPlus = (ImageView) convertView.findViewById(R.id.row_plus);
		rowPlus.setTag(groupPosition);
		rowPlus.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		        Log.i("ExpandableListAdapter", "Zobrazit detail");		        
		        showEventDetails((Integer)v.getTag());
		    }
		});
		
		ImageView rowLogin = (ImageView) convertView.findViewById(R.id.row_login);
		rowLogin.setTag(groupPosition);
		rowLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loginToEvent((Integer)v.getTag());
				
			}
		});
		
		return convertView;
	}
	
	 @Override
	 public void onGroupCollapsed(int groupPosition) {
	   super.onGroupCollapsed(groupPosition);
	 }

	 @Override
	 public void onGroupExpanded(int groupPosition) {
	   super.onGroupExpanded(groupPosition);
	 }

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	
	protected void showEventDetails(int groupPosition){
		EventFragment eventDetail = new EventFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("id", getGroup(groupPosition).id);		
		eventDetail.setArguments(bundle);
		
		FragmentManager fragmentManager = ((Activity) mContext).getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.content_frame, eventDetail);
		transaction.addToBackStack(null);
		transaction.commit();		
	}
	
	protected void loginToEvent(int groupPosition){
		BaseAuthFragment loginFrag = new AuthFragmentEvent();
		Bundle bundle = new Bundle();
		bundle.putInt("id", getGroup(groupPosition).id);		
		loginFrag.setArguments(bundle);
		
		FragmentManager fragmentManager = ((Activity) mContext).getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.content_frame, loginFrag);
		transaction.addToBackStack(null);
		transaction.commit();		
	}

}
