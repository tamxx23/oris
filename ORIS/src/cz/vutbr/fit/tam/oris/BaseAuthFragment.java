package cz.vutbr.fit.tam.oris;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import cz.vutbr.fit.tam.oris.OrisAPI.Method;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public abstract class BaseAuthFragment extends Fragment implements OrisAPIListener {

	//TODO Vytvořit checkButton pro "zapamatovat přihlášení" a implementovat SharedPreferences
	
	OrisAPI orisAPI;
	protected int eventId = -1;
	private String regNumber = "88X8000";
	View loginView;
	
	private SharedPreferences mSharedPref;
	
	/** Konstruktor */
	public BaseAuthFragment() {
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		loginView = inflater.inflate(R.layout.fragment_login, container, false);
		Bundle bundle = this.getArguments();	
		//if (!bundle.isEmpty()) {
		if (bundle != null) {
			eventId = bundle.getInt("id");
		}
		
		Button login = (Button) loginView.findViewById(R.id.bLoginNext);
		//TODO smazat po doimplementaci přihlašování
		//login.setEnabled(false);
		//TextView tv = (TextView) loginView.findViewById(R.id.password);
		//tv.setEnabled(false);
		//tv = (TextView) loginView.findViewById(R.id.login);
		//tv.setEnabled(false);
		// po sem smazat
		
		mSharedPref = getActivity().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		EditText etLogin = (EditText) loginView.findViewById(R.id.login);
		EditText etRegNum = (EditText) loginView.findViewById(R.id.EditTextRegNumber);
		EditText etPasswd = (EditText) loginView.findViewById(R.id.password);
		etLogin.setText(mSharedPref.getString("Login", ""));
		etRegNum.setText(mSharedPref.getString("RegNumber", ""));
		etPasswd.setText(mSharedPref.getString("Password", ""));
		
		login.setOnClickListener(new Button.OnClickListener(){
			public void onClick(View v) {
				next();
			}
		});
		
		orisAPI = new OrisAPI(this.getActivity(), this);		
				
		getActivity().setTitle("Přihlášení");
		return loginView;
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_filter).setVisible(false);
        menu.findItem(R.id.action_order_by).setVisible(false);
        menu.findItem(R.id.action_order).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }
	
    private void next(){
    	EditText regNum = (EditText) loginView.findViewById(R.id.EditTextRegNumber);    	
    	regNumber = regNum.getText().toString();
    	CheckBox remember = (CheckBox) loginView.findViewById(R.id.cbRemember);
    	EditText edUserName = (EditText) loginView.findViewById(R.id.login);
    	EditText edPassword = (EditText) loginView.findViewById(R.id.password);
    	
    	// zapamatování údajů
    	if (remember.isChecked()) {
    		SharedPreferences.Editor editor = mSharedPref.edit();
 		   	editor.putString("Login", edUserName.getText().toString());
 		   	editor.putString("RegNumber", regNumber);
 		   	editor.putString("Password", edPassword.getText().toString());
 		   	editor.commit();
    	}
    	
    	try {
			orisAPI.getUser(regNumber);
		
		} catch (Exception e) {
			Toast.makeText(this.getActivity().getApplicationContext(), "Nemáš připojení k INTERNETu", Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
		Toast.makeText(getActivity().getApplicationContext(), "Stisknuto Přihlásit", Toast.LENGTH_SHORT).show();
    }
    
	/** 
	 * Metoda z OrisAPIListener, ktera se asynchrone vola z OrisAPI se stazenymi daty.
	 * Nacte data do databaze a pote vola fillLayout()
     * @param data Data z weboveho API jako JSONObject
     * @param method Typ dotazu (OrisAPI.Method)
     */
	@Override
	public void onDownloadComplete(JSONObject data, Method method) {
		Log.i("onDownloadComplete",data.toString());
		switch(method){
			case GET_USER:				
				try {
					
					if (data.optJSONObject("Data") != null){
						EditText edUserName = (EditText) loginView.findViewById(R.id.login);
				    	EditText edPassword = (EditText) loginView.findViewById(R.id.password); 
				    	
						JSONObject dataObj = data.getJSONObject("Data");
						Log.i("GET_USER",dataObj.toString());					
	
						int userId = dataObj.getInt("ID");
						String firstName = dataObj.getString("FirstName");
						String lastName = dataObj.getString("LastName");	
						String license = dataObj.getString("RefLicenceOB");	
						
						new User(edUserName.getText().toString(), edPassword.getText().toString(), firstName, lastName, userId, license);
												
						nextFragment();						
					}
					else
						Toast.makeText(getActivity().getApplicationContext(), "Chybné registrační číslo.", Toast.LENGTH_SHORT).show();
				} catch (JSONException e) {					
					e.printStackTrace();
				}		
				break;	
			
			case UNABLE_RETRIEVE_DATA:
				
			break;
				
			default:
				break;
		}		
	}
	
	abstract protected void nextFragment(); 
}
