/**
 * 
 */
package cz.vutbr.fit.tam.oris;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;


/** Zajistuje komunikaci s webovym API. Pri dotazu na server se spusti vlakno pro odeslani pozadavku
 * a stazeni odpovedi. Po odpovedi serveru je volana metoda z onDownloadComplete() OrisAPIListener, 
 * kde preda vysledek.
 * Pro pouziti je potreba svoji tridu implementovat jako intreface OrisAPIListener 
 * s metodou onDownloadComplete().
 */
public class OrisAPI{

	private static final String DEBUG_TAG = "ORISAPI";
	/** Kontext aktivity */
	Context context;
	/** Listener aktivity */
	OrisAPIListener callback;
	
	/** Vycet dotazu na API. Vraci se spolecne s vysledkem. */
	public enum Method{
		GET_EVENT_LIST,
		GET_EVENT,
		GET_EVENT_ENTRIES,
		GET_EVENT_SERVICE_ENTRIES,
		GET_EVENT_RESULTS,
		GET_USER,
		GET_REGISTRATION,
		GET_CSOS_CLUB_LIST,
		GET_CLUB,
		GET_CLUB_USERS,
		GET_VALID_CLASSES,
		CREATE_ENTRY,
		DELETE_ENTRY,
		CREATE_SERVICE_ENTRY,
		DELETE_SERVICE_ENTRY,
		UNABLE_RETRIEVE_DATA
	}
	
	public enum HTTPMethod{
		GET,
		POST
	}
	
	/** Vycet druhu sportu */
	public enum Sport{
		ALL_SPORTS,
		OB,
		LOB,
		MTBO
	}
	
	/*public enum Level{
		ALL,
		MCR,
		ZA,
		ZB,
		V,
		CPS,
		CP		
	}*/
	
	/** Listener pro vysledek z vlakna, ktere ziska data od weboveho API.
	 * Preposilaji se do aktivity, ktera pracuje vysledek.
	 */
	DataDownloaderListener DCallback= new  DataDownloaderListener(){
        @Override
        public void onDownloadComplete(String data,Method method) {
        	JSONObject jsonData = null;
        	//Log.d(DEBUG_TAG,data);
        	switch(method){
        		case GET_EVENT_LIST:
        		case GET_EVENT:
        		case GET_CLUB:
        		case GET_CSOS_CLUB_LIST:
    			case GET_EVENT_ENTRIES:
    			case GET_EVENT_RESULTS:    				
    			case GET_EVENT_SERVICE_ENTRIES:    				
    			case GET_REGISTRATION:    				
    			case GET_USER:
    			case GET_CLUB_USERS:
    			case GET_VALID_CLASSES:
    			case CREATE_ENTRY:
    			case DELETE_ENTRY:
    			case CREATE_SERVICE_ENTRY:
    			case DELETE_SERVICE_ENTRY:
        			// vytvoreni JSONObjectu z prijatych dat.
	        		try {
	        			if (data != null) {
	        				jsonData = new JSONObject(data);  
	        				//Log.d(DEBUG_TAG+"_datas",jsonData.toString());
	        			}
		     		}      		
		     		catch (JSONException e) {
		     			e.printStackTrace();
		     		}
        		break;
    				
        		case UNABLE_RETRIEVE_DATA:
        		break;			
        	}	
	        callback.onDownloadComplete(jsonData, method);
        }
    };	
	
    /** Konstruktor
     * @param mContext kontext aktivity
     * @param mCallback callback aktivity
     */
	public OrisAPI(Context mContext, OrisAPIListener mCallback){				
		this.context = mContext;
		this.callback = mCallback;
	}
	
	/** Interface, ktery deklaruje metodu volanou z pracovniho vlakna pri prijeti vysledku ze serveru. */
	protected interface DataDownloaderListener{
		/**Metoda vraci data z odpovedi a typ pozadavku 
		 * @param data odpoved serveru
		 * @param method typ dotazu na server
		 * */
		public void onDownloadComplete(String data,Method method);
	}	
	
	/** Interface, ktery deklaruje metodu volanou z pracovniho vlakna pri prijeti vysledku ze serveru. */
	/*protected interface DataInsertListener{
		
		//public void onDownloadComplete(String data,Method method);
		public void onInsertComplete();
	}*/
	
	
	/** Dotaz na seznam zavodu.
	 * @param sport 		id sportu
	 * @param rg 			zkratka regionu
	 * @param name 			cast nazvu zavodu
	 * @param dateFrom 		datum od ve form�tu RRRR-MM-DD
	 * @param dateTo 		datum do ve form�tu RRRR-MM-DD
	 * @throws Exception 	Vyhodi se vyjimka pokud se nelze pripojit k siti.
	 */
	public void getEventList(Sport sport, String rg, String name, String dateFrom, String dateTo, boolean allEvents) throws Exception{
		//String request = new String("http://oris.orientacnisporty.cz/API/?format=json&method=getEventList");
		String request = new String("format=json&method=getEventList");
		//if(allEvents){
			request += "&all=1";
		//}
		
		if(sport != Sport.ALL_SPORTS)	{		
			switch(sport){
			case OB:
				request += "&sport=1";
				break;
				
			case LOB:
				request += "&sport=2";
				break;
				
			case MTBO:
				request += "&sport=3";
				break;
			default:
				break;
			}
			
		}
		/*if(level != Level.ALL)	{		
			switch(level){
			case MCR:
				request += "&level=1";
				break;
				
			case OF:
				request += "&level=2";
				break;
				
			case OZ:
				request += "&level=3";
				break;
			default:
				break;
			}
			
		}*/
		if(rg.length() != 0)			
			request += "&rg="+rg;		
		if(name.length() != 0)			
			request += "&name="+name;
		if(dateFrom.length() != 0){	
			if(!checkDate(dateFrom))
				throw new Exception("Špatně formátovaný datum.");
			
			request += "&datefrom="+dateFrom;
			//Log.d(DEBUG_TAG,"dateFrom ok");
		}
		if(dateTo.length() != 0){
			if(!checkDate(dateTo))
				throw new Exception("Špatně formátovaný datum.");
			
			request += "&dateto="+dateTo;
			//Log.d(DEBUG_TAG,"dateTo ok");
		}
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_EVENT_LIST);
	}
	
	public void getEvent(String id) throws Exception{
	
		String request = new String("format=json&method=getEvent");
		
		if(id.length() != 0 )
			request += "&id="+id;
		else
			throw new Exception("Musí být vybrán závod.");
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_EVENT);
	}
	
	public void getEventEntries(String eventId, String classId, String className, String clubId, String entryStop, String entryStopOut) throws Exception{
		String request = new String("format=json&method=getEventEntries");
		if(eventId.length() != 0 )
			request += "&eventid="+eventId;
		else
			throw new Exception("Musí být vybrán závod.");
		
		if((clubId.length() != 0) && ((classId.length() != 0) || (className.length() != 0))){
			throw new Exception("Nelze současně filtrovat přihláky podle klubu a kategorie.");
		}
		if(classId.length() != 0)
			request += "&classid="+classId;
		if(className.length() != 0)
			request += "&classname="+className;
		if(clubId.length() != 0)
			request += "&clubid="+clubId;
		if(entryStop.length() != 0)
			request += "&entrystop="+entryStop;
		if(entryStopOut.length() != 0)
			request += "&entrystopout="+entryStopOut;
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_EVENT_ENTRIES);
	}
	
	public void getEventResults(String eventId, String classId, String className, String clubId) throws Exception{
		String request = new String("format=json&method=getEventResults");
		if(eventId.length() != 0 )
			request += "&eventid="+eventId;
		else
			throw new Exception("Musí být vybrán závod.");
		
		if((clubId.length() != 0) && ((classId.length() != 0) || (className.length() != 0))){
			throw new Exception("Nelze současně filtrovat výsledky podle klubu a kategorie.");
		}
		if(classId.length() != 0)
			request += "&classid="+classId;
		if(className.length() != 0)
			request += "&classname="+className;
		if(clubId.length() != 0)
			request += "&clubid="+clubId;
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_EVENT_RESULTS);
	}	
	
	public void getEventServiceEntries(String eventId, String clubId) throws Exception{
		String request = new String("format=json&method=getEventServiceEntries");
		if(eventId.length() != 0 )
			request += "&eventid="+eventId;
		else
			throw new Exception("Musí být vybrán závod.");		
		
		if(clubId.length() != 0)
			request += "&clubid="+clubId;
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_EVENT_SERVICE_ENTRIES);
	}
	
	public void getUser(String rgNum) throws Exception{
		String request = new String("format=json&method=getUser");
		if(rgNum.length() != 0 )
			request += "&rgnum="+rgNum;
		else
			throw new Exception("Musí být zadáno registrační číslo.");
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_USER);
	}
	
	
	public void getRegistration(Sport sport, String year) throws Exception{
		String request = new String("format=json&method=getRegistration");
				
		switch(sport){				
		case OB:
			request += "&sport=1";
			break;
			
		case LOB:
			request += "&sport=2";
			break;
			
		case MTBO:
			request += "&sport=3";
			break;
			
		default:
			throw new Exception("Není vybrán sport.");
		}
		
		if(year.length() == 4 )
			request += "&year="+year;
		else
			throw new Exception("Není vybrán rok.");
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_REGISTRATION);
	}	
		
	public void getCSOSClubList() throws Exception{
		String request = new String("format=json&method=getCSOSClubList");	
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_CSOS_CLUB_LIST);
	}
	
	public void getClub(String clubId) throws Exception{
		String request = new String("format=json&method=getClub");			
		
		if(clubId.length() != 0)
			request += "&id="+clubId;
		else
			throw new Exception("Není vybrán club.");
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_CLUB);
	}
		
	/** Seznam clenstvi v klubech pro konkretniho uzivatele.
	 * @param userId 		id sportu
	 * @param date 			datum od ve formatu RRRR-MM-DD	
	 * @throws Exception	vyjimka pokud neni pripojeni k siti, neni zadano ide uzivatele
	 * 						nebo neni datum ve spravnem formatu
	 */
	public void getClubUsers(String userId,String date) throws Exception{
		String request = new String("format=json&method=getClubUsers");			
		
		if(userId.length() != 0)
			request += "&user="+userId;
		else
			throw new Exception("Není id uživatele.");
		
		if(date.length() != 0){
			if(!checkDate(date))
				throw new Exception("Špatně formátovaný datum.");
			
			request += "&dateto="+date;			
		}
		
		String url = makeURL(request);
		//Log.i("clubUser", url);
		this.downloadData(url, Method.GET_CLUB_USERS);
	}
	
	/** Seznam kategorii platnych pro konkretniho clena klubu v danem zavode.
	 * Pro kazdy zavod jsou unikatni. 
	 * @param clubUser 		id clenstvi v klubu
	 * @param comp 			id zavodu	
	 * @throws Exception	vyjimka pokud neni pripojeni k siti, neni zadano ide uzivatele, neni zadano id zavodu 						
	 */
	public void getValidClasses(String clubUser, String comp) throws Exception{
		String request = new String("format=json&method=getValidClasses");			
		
		if(clubUser.length() != 0)
			request += "&clubuser="+clubUser;
		else
			throw new Exception("Není zadáno id členství v klubu.");
		
		if(comp.length() != 0)
			request += "&comp="+comp;
		else
			throw new Exception("Není zadáno id závodu.");
		
		String url = makeURL(request);
		//Log.i("ValidClasses", url);
		this.downloadData(url, Method.GET_VALID_CLASSES);
	}
	
	/** Prihlaseni zavodnika na zavod (POST pozadavek)
	 * Povinne parametry
	 * @param username 			prihlasovaci jmeno
	 * @param password 			heslo
	 * @param clubuser 			id clenstvi v klubu
	 * @param category 			kategorie v zavodu
	 * 
	 * Nepovinne paramentry
	 * @param si 				SI cip
	 * @param note 				poznamka
	 * @param clubnote 			klubova poznamka
	 * @param requested_start	pozadovany start	
	 * @param rent_si 			zapujceni cipu
	 * @param stageX 			prihlaska na x-tou etapu vycedenich zavodu
	 * @param entrystatus 		termin prihlasek	
	 * @throws Exception	vyjimka pokud neni pripojeni k siti, neni zadano ide uzivatele, neni zadano id zavodu 						
	 */
	public void createEntry(String username, String password, String clubuser, 
			String category, String si, String note, String clubnote, String requested_start,
			String rent_si, String stageX, String entrystatus) throws Exception{
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();		
		
		//autorizace
		if(username.length() != 0)
			params.add(new BasicNameValuePair("username", username));
		else
			throw new Exception("Není zadán login uživatele.");		
		if(password.length() != 0)
			params.add(new BasicNameValuePair("password", password));
		else
			throw new Exception("Není zadáno heslo uživatele.");
		
		//přihláška
		if(clubuser.length() != 0)
			params.add(new BasicNameValuePair("clubuser", clubuser));
		else
			throw new Exception("Není zadáno id členství v klubu.");		
		if(category.length() != 0)
			params.add(new BasicNameValuePair("class", category));
		else
			throw new Exception("Není zadána kategorie závodu.");
		
		if(si.length() != 0)
			params.add(new BasicNameValuePair("si", si));
		if(note.length() != 0)
			params.add(new BasicNameValuePair("note", note));
		if(clubnote.length() != 0)
			params.add(new BasicNameValuePair("clubnote", clubnote));
		if(requested_start.length() != 0)
			params.add(new BasicNameValuePair("requested_start", requested_start));
		if(rent_si.length() != 0)
			params.add(new BasicNameValuePair("rent_si", rent_si));
		if(stageX.length() != 0)
			params.add(new BasicNameValuePair("stageX", stageX));
		if(entrystatus.length() != 0)
			params.add(new BasicNameValuePair("entrystatus", entrystatus));
		
		String query="";
		try{
			query = getPostQuery(params);
		}catch(UnsupportedEncodingException e){
			throw new Exception("Problém s kodovanim dotazu.");
		}
		
		String url = makeURL("format=json&method=createEntry");
		Log.i("createEntry", url);
		Log.i("createEntry", query);
		this.downloadData(url, query, Method.CREATE_ENTRY);
	}
	
	/** Odhlaseni zavodnika ze zavodu (POST pozadavek)
	 * Povinne parametry
	 * @param username 			prihlasovaci jmeno
	 * @param password 			heslo
	 * @param entryid 			id prihlasky
	 * @throws Exception	vyjimka pokud neni pripojeni k siti, neni zadano ide uzivatele, neni zadano id zavodu 						
	 */
	public void deleteEntry(String username, String password, String entryid) throws Exception{
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();		
		
		//autorizace
		if(username.length() != 0)
			params.add(new BasicNameValuePair("username", username));
		else
			throw new Exception("Není zadán login uživatele.");		
		if(password.length() != 0)
			params.add(new BasicNameValuePair("password", password));
		else
			throw new Exception("Není zadáno heslo uživatele.");
		if(entryid.length() != 0)
			params.add(new BasicNameValuePair("entryid", entryid));
		else
			throw new Exception("Není zadáno id prihlasky.");		
		
		String query="";
		try{
			query = getPostQuery(params);
		}catch(UnsupportedEncodingException e){
			throw new Exception("Problém s kodovanim dotazu.");
		}
		
		String url = makeURL("format=json&method=deleteEntry");
		Log.i("deleteEntry", url);
		Log.i("deleteEntry", query);
		this.downloadData(url, query, Method.DELETE_ENTRY);
	}
	
	/** Objednání doplňkové služby (POST pozadavek)
	 * Povinne parametry
	 * @param username 			prihlasovaci jmeno
	 * @param password 			heslo
	 * @param entryid 			id prihlasky
	 * @throws Exception	vyjimka pokud neni pripojeni k siti, neni zadano ide uzivatele, neni zadano id zavodu 						
	 */
	public void createServiceEntry(String username, String password, String clubuser, 
			String service, String qty, String note) throws Exception{
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();		
		
		//autorizace
		if(username.length() != 0)
			params.add(new BasicNameValuePair("username", username));
		else
			throw new Exception("Není zadán login uživatele.");		
		if(password.length() != 0)
			params.add(new BasicNameValuePair("password", password));
		else
			throw new Exception("Není zadáno heslo uživatele.");
		if(clubuser.length() != 0)
			params.add(new BasicNameValuePair("clubuser", clubuser));
		else
			throw new Exception("Není zadáno id členství v klubu.");		
		if(service.length() != 0)
			params.add(new BasicNameValuePair("service", service));
		else
			throw new Exception("Není zadána kategorie závodu.");
		if(qty.length() != 0)
			params.add(new BasicNameValuePair("qty", qty));
		else
			throw new Exception("Není zadáno množství.");
		
		if(note.length() != 0)
			params.add(new BasicNameValuePair("note", note));
		
		String query="";
		try{
			query = getPostQuery(params);
		}catch(UnsupportedEncodingException e){
			throw new Exception("Problém s kodovanim dotazu.");
		}
		
		String url = makeURL("format=json&method=createServiceEntry");
		Log.i("createServiceEntry", url);
		Log.i("createServiceEntry", query);
		//this.downloadData(url, query, Method.CREATE_SERVICE_ENTRY);
	}
	
	/** Zrušení objednávky doplňkové služby (POST pozadavek)
	 * Povinne parametry
	 * @param username 			prihlasovaci jmeno
	 * @param password 			heslo
	 * @param entryid 			id prihlasky
	 * @throws Exception	vyjimka pokud neni pripojeni k siti, neni zadano ide uzivatele, neni zadano id zavodu 						
	 */
	public void createServiceEntry(String username, String password, String serviceentryid) throws Exception{
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();		
		
		//autorizace
		if(username.length() != 0)
			params.add(new BasicNameValuePair("username", username));
		else
			throw new Exception("Není zadán login uživatele.");		
		if(password.length() != 0)
			params.add(new BasicNameValuePair("password", password));
		else
			throw new Exception("Není zadáno heslo uživatele.");
		if(serviceentryid.length() != 0)
			params.add(new BasicNameValuePair("serviceentryid", serviceentryid));
		else
			throw new Exception("Není zadáno id objednávky doplňkové služby.");			
		
		String query="";
		try{
			query = getPostQuery(params);
		}catch(UnsupportedEncodingException e){
			throw new Exception("Problém s kodováním dotazu.");
		}
		
		String url = makeURL("format=json&method=deleteServiceEntry");
		Log.i("deleteServiceEntry", url);
		Log.i("deleteServiceEntry", query);
		//this.downloadData(url, query, Method.DELETE_SERVICE_ENTRY);
	}
	
	/** Vytvoreni http dotazu.  
	 * @param request Fragment dotazu
	 * @throws URISyntaxException 
	 */
	protected String makeURL(String request) throws URISyntaxException{		
		URI uri = new URI(
    		    "http", 
    		    "oris.orientacnisporty.cz", 
    		    "/API/",
    		    request,
    		    null
    		    );
		
		//Log.d(DEBUG_TAG,uri.toASCIIString());
		return uri.toASCIIString();
	}
	
	/** Kotrola formatu data.  
	 * @param date datum
	 */
	protected boolean checkDate(String date){		
		return Pattern.matches("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]",date);
	}
	
	private String getPostQuery(List<NameValuePair> params) throws UnsupportedEncodingException
	{
	    StringBuilder result = new StringBuilder();
	    boolean first = true;

	    for (NameValuePair pair : params)
	    {
	        if (first)
	            first = false;
	        else
	            result.append("&");

	        result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
	        result.append("=");
	        result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
	    }

	    return result.toString();
	}
	
	/** Kotrola formatu data.
	 * @param url http dotaz  
	 */
	protected void downloadData(String url, Method method) throws Exception{
		ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {	
			DataDownloader downloader = new DataDownloader(this.DCallback, method, HTTPMethod.GET);		    		    	 				    	
			downloader.execute(url);					
		   //	Log.d(DEBUG_TAG,"Network ok.");
		} else {
			//Log.d(DEBUG_TAG,"No network connection available.");
			throw new Exception("Není k dispozici internetové připojení.");		 
		}
	}
	
	protected void downloadData(String url, String query, Method method) throws Exception{
		ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {	
			DataDownloader downloader = new DataDownloader(this.DCallback, method,  HTTPMethod.POST);		    		    	 				    	
			downloader.execute(url, query);					
		   //	Log.d(DEBUG_TAG,"Network ok.");
		} else {
			//Log.d(DEBUG_TAG,"No network connection available.");
			throw new Exception("Není k dispozici internetové připojení.");		 
		}
	}
	
	/** Trida posila pozadavek a prijima odpovede ze serveru ORIS. Je spoustena ve vlastnim vlakne, 
	 * takze nezamrzne gui. Pri prijimani pozadavku zobrazi progres bar
	 */
	private class DataDownloader extends AsyncTask<String, Void, String> {
		
		/** Progres dialog pri stahovani */
		ProgressDialog progressDialog;
		/** Listener tridy OrisAPI */
		DataDownloaderListener callback;
		/** Typ dotazu na server */
		Method method;
		/** Typ http dotazu na server */
		HTTPMethod httpMethod;
		List<NameValuePair> params;
		
		/** Konstruktor	    * 
	    * @param mCallback Callback aktivity
	    * @param mMethod Typ dotazu na server
	    */
		public DataDownloader(DataDownloaderListener mCallback, Method mMethod, HTTPMethod mHttpMethod){
			this.callback = mCallback;
			this.method = mMethod;
			this.httpMethod = mHttpMethod;
		}	
		
		/** Spousti metodu v novem vlakne.
		* @param url http dotaz
		*/
        @Override
        protected String doInBackground(String ... url) { 
        	// pokud se nepodari stazeni, nastavi se method do UNABLE_RETRIEVE_DATA,
        	// ktere se pak dostane do aktivity.
            try {
            	switch(this.httpMethod){
            		case GET:
            			return downloadGET(url[0]);            			
            		
            		case POST:
            			return downloadPOST(url[0], url[1]);
            			
            		default:
            			return "";
            	}
            } catch (IOException e) {
            	method = Method.UNABLE_RETRIEVE_DATA;
                return "Unable to retrieve web page. URL may be invalid.";
            }            
        }        
        
        /** Pred spustenim prace v novem vlakne zobrazi progress bar.*/		
        @Override
        protected void onPreExecute() {
        	if (progressDialog == null) {
        		progressDialog = new ProgressDialog(context);
                progressDialog.setMessage(context.getResources().getString(R.string.downloading));
                progressDialog.show();
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setCancelable(false);
        	}
        }
        
        /** Po stazeni dat zrusi progress bar a vola callback v OrisAPI.
		* @param result odpoved serveru
		*/
        @Override
        protected void onPostExecute(String result) {        	      	
        	callback.onDownloadComplete(result, this.method);
        	if (progressDialog.isShowing()) {
        		progressDialog.dismiss();
            }
        }
        
        /** Po stazeni dat zrusi progress bar a vola callback v OrisAPI.
		* @param urlString http dotaz
		* @throws IOException chyba pri spojeni k serveru
		*/
        protected String downloadGET(String urlStr) throws IOException {        	
            BufferedInputStream in = null;
            try { 
                URL url = new URL(urlStr);
                
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();               
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();                
                in = new BufferedInputStream(conn.getInputStream());
                
                Log.d(DEBUG_TAG, "The response is: " + response);
                
                // Prevedeni InputStream na String 
                String data = readData(in);                
                return data;
            } finally {
                if (in != null) {
                    in.close();
                }                
            }
        }
        
        protected String downloadPOST(String urlStr, String query) throws IOException {
        
        	BufferedInputStream in = null;
	        try {	
		        URL url = new URL(urlStr);
		        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        conn.setReadTimeout(10000);
		        conn.setConnectTimeout(15000);
		        conn.setRequestMethod("POST");
		        conn.setDoInput(true);
		        conn.setDoOutput(true);
	        	
		        OutputStream os = conn.getOutputStream();
		        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
		        writer.write(query);
		        writer.flush();
		        writer.close();
		        os.close();
	
		        conn.connect();
		        
		        int response = conn.getResponseCode();
		        Log.d(DEBUG_TAG, "The response is: " + response);	        
		       
	            in = new BufferedInputStream(conn.getInputStream());	            
	            // Prevedeni InputStream na String 
	            String data = readData(in);                
	            return data;
	            
	        } finally {
	            if (in != null) {
	                in.close();
	            }                
	        }
        	
        	/*HttpClient client = new DefaultHttpClient();
        	HttpPost post = new HttpPost(urlStr);
        	post.setEntity(new UrlEncodedFormEntity(params));
        	
        	HttpResponse httpresponse = client.execute(post);
        	
        	String responseText = null;        	
        	responseText = EntityUtils.toString(httpresponse.getEntity());
        	 return responseText;*/
        	
        }
        
        /** Prevedeni prijatych dat na string.
		* @param stream Odpoved serveru.
		* @throws IOException chyba pri spojeni k serveru
		*/
        protected String readData(BufferedInputStream stream) throws IOException {
        	byte[] content = new byte[1024];

        	int bytesRead=0;
        	String strData = new String(""); 
        	while( (bytesRead = stream.read(content)) != -1){         		 
        		strData += new String(content, 0, bytesRead);
        	}
            return strData;
        }
    }
}
