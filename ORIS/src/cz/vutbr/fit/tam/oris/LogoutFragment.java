package cz.vutbr.fit.tam.oris;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import cz.vutbr.fit.tam.oris.OrisAPI.Method;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LogoutFragment extends Fragment implements OrisAPIListener {

	//TODO Vytvořit checkButton pro "zapamatovat přihlášení" a implementovat SharedPreferences
	
	OrisAPI orisAPI;
	private int eventId;
	private String regNumber;
	View logoutView;
	private OrisHelper helper;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		logoutView = inflater.inflate(R.layout.fragment_login, container, false);
		Bundle bundle = this.getArguments();		
		eventId = bundle.getInt("id");
		
		Button logout = (Button) logoutView.findViewById(R.id.bLoginNext);
		logout.setGravity(Gravity.CENTER);
		logout.setText(R.string.logout);
	
		logout.setOnClickListener(new Button.OnClickListener(){
			public void onClick(View v) {
				logout();
			}
		});
		
		orisAPI = new OrisAPI(this.getActivity(), this);		
		this.helper = new OrisHelper(this.getActivity());
		
		try {
			helper.deleteEventEntries(eventId);
			this.orisAPI.getEventEntries(Integer.toString(eventId),"","","","","");
		} catch (Exception e) {
			Toast.makeText(this.getActivity().getApplicationContext(), "Nemáš připojení k INTERNETu", Toast.LENGTH_SHORT).show();
		}
		
		getActivity().setTitle("Přihlášení");
		return logoutView;
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_filter).setVisible(false);
        menu.findItem(R.id.action_order_by).setVisible(false);
        menu.findItem(R.id.action_order).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }
	
    private void logout(){
    	EditText regNum = (EditText) logoutView.findViewById(R.id.EditTextRegNumber);    	
    	EditText etUserName = (EditText) logoutView.findViewById(R.id.login);
    	EditText etPassword = (EditText) logoutView.findViewById(R.id.password); 
    	
    	regNumber = regNum.getText().toString();
    	
    	Cursor entry = this.helper.getEventEntry(eventId, regNumber);
    	entry.moveToFirst();
    	
    	if (entry.getCount() != 0){
    		int entryID = entry.getInt(0);
    		try {
    			
    			Log.i("LogoutFragment", "entryID: "+ entryID);
        		orisAPI.deleteEntry(etUserName.getText().toString(), etPassword.getText().toString(), Integer.toString(entryID));
    		
    		} catch (Exception e) {
    			Toast.makeText(this.getActivity().getApplicationContext(), "Nemáš připojení k INTERNETu", Toast.LENGTH_SHORT).show();
    			e.printStackTrace();
    		}
    	}
    	else
    		Toast.makeText(this.getActivity().getApplicationContext(), "Nejsi přihlášený v závodě.", Toast.LENGTH_SHORT).show();
    }
    
	/** 
	 * Metoda z OrisAPIListener, ktera se asynchrone vola z OrisAPI se stazenymi daty.
     * @param data Data z weboveho API jako JSONObject
     * @param method Typ dotazu (OrisAPI.Method)
     */
	@Override
	public void onDownloadComplete(JSONObject data, Method method) {
		Log.i("onDownloadComplete",data.toString());
		switch(method){
		case GET_EVENT_ENTRIES:				
			try {
				// nacteni prihlasek zavodu do databaze
				if(data.has("Data") && (data.optJSONObject("Data") != null)){
					JSONObject dataObj = data.getJSONObject("Data");
					Iterator<String> keys = dataObj.keys();
					Log.i("EventEntriesFragment",""+dataObj.length());										
					while(keys.hasNext()){
						int entryId=0, classId=0, si=0, clubId=0, fee=0, rentSI=0;
						String name = "", regNo="", licence=""; 
						String key = keys.next();
						if(dataObj.getJSONObject(key).has("ID") && (dataObj.getJSONObject(key).optInt("ID") != 0))
							entryId = dataObj.getJSONObject(key).getInt("ID");
						if(dataObj.getJSONObject(key).has("Name"))
							name = dataObj.getJSONObject(key).getString("Name");
						if(dataObj.getJSONObject(key).has("ClassID") && (dataObj.getJSONObject(key).optInt("ClassID") != 0))
							classId = dataObj.getJSONObject(key).getInt("ClassID");
						if(dataObj.getJSONObject(key).has("ClubID") && (dataObj.getJSONObject(key).optInt("ClubID") != 0))
							clubId = dataObj.getJSONObject(key).getInt("ClubID");
						if(dataObj.getJSONObject(key).has("SI") && (dataObj.getJSONObject(key).optInt("SI") != 0))
							si = dataObj.getJSONObject(key).getInt("SI");
						if(dataObj.getJSONObject(key).has("Fee") && (dataObj.getJSONObject(key).optInt("Fee") != 0))
							fee = dataObj.getJSONObject(key).getInt("Fee");
						if(dataObj.getJSONObject(key).has("RegNo"))
							regNo = dataObj.getJSONObject(key).getString("RegNo");
						if(dataObj.getJSONObject(key).has("Licence") && (dataObj.getJSONObject(key).getString("Licence") != "null"))
							licence = dataObj.getJSONObject(key).getString("Licence");
						if(dataObj.getJSONObject(key).has("RentSI") && (dataObj.getJSONObject(key).optInt("Fee") != 0))
							rentSI = dataObj.getJSONObject(key).getInt("RentSI");
						
						if (!(this.helper.recordExists("entries", entryId))) {
							this.helper.insertEntry(entryId, name, classId, clubId, si, fee, regNo, licence, rentSI, eventId);							
						}
						else{
							this.helper.updateEntry(entryId, name, classId, clubId, si, fee, regNo, licence, rentSI, eventId);	
						}
					}	
				}					
				
			} catch (JSONException e) {					
				e.printStackTrace();
			}	
			break;
			
		case DELETE_ENTRY:
			// zjistim jestli probehlo odhlaseni spravne, pokud ano smazu id prihlasky
			try {
				Log.i("DELETE_ENTRY",data.toString());
				String status =data.getString("Status");
				if(status.equals("OK")){										
					Toast.makeText(this.getActivity().getApplicationContext(), "Odlášení ze závodu proběhlo úspěšně.", Toast.LENGTH_SHORT).show();	
					getActivity().getFragmentManager().popBackStack();
				}
				else
					Toast.makeText(this.getActivity().getApplicationContext(), status, Toast.LENGTH_SHORT).show();	
			} catch (JSONException e) {					
				e.printStackTrace();
			}				
			
			break;
			
			case UNABLE_RETRIEVE_DATA:
				
			break;
				
			default:
				break;
		}		
	}
}

