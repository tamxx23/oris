package cz.vutbr.fit.tam.oris;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import cz.vutbr.fit.tam.oris.OrisAPI.Method;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class EventLoginFragment extends Fragment implements OrisAPIListener, OnItemSelectedListener{

	private int eventId;	
	private OrisAPI orisAPI;
	User user;
	
	//pouze pro testovani, prihlasim se na zavod a ulozim si id prihlasky, ktere pak pouziju pro odhlaseni.
	private int entryId; 
	
	private List<ClubUser> clubUsers = new ArrayList<EventLoginFragment.ClubUser>();
	private List<Category> categories = new ArrayList<EventLoginFragment.Category>();
	
	private View rootView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
				
		rootView = inflater.inflate(R.layout.fragment_event_login, container, false);
		Bundle bundle = this.getArguments();		
		eventId = bundle.getInt("id");
		
		Button login = (Button) rootView.findViewById(R.id.buttonEventLogin);		
		login.setOnClickListener(new Button.OnClickListener(){
			public void onClick(View v) {
				eventLogin();
			}
		});
		
		login = (Button) rootView.findViewById(R.id.buttonBack);		
		login.setOnClickListener(new Button.OnClickListener(){
			public void onClick(View v) {
				back();
			}
		});
		
		Spinner spinner = (Spinner) rootView.findViewById(R.id.spinnerRegNum);
		spinner.setOnItemSelectedListener(this);
		
		spinner = (Spinner) rootView.findViewById(R.id.spinnerCategory);
		spinner.setOnItemSelectedListener(this);
		
		orisAPI = new OrisAPI(this.getActivity(), this);		
		
		try {
			user = User.getUser();
			// odeslani pozadavku na API pro vypis vsech clenstvi uzivatele v klubech
			orisAPI.getClubUsers(Integer.toString(user.getuserId()),"");						
		} catch (Exception e) {
			Toast.makeText(this.getActivity().getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
		
		getActivity().setTitle("Přihlášení na závod");
		return rootView;
	}
	
	private void eventLogin(){
		// prihlasim se do zavodu dle vybraneho registracniho cisla a kategorie
		try {
			int clubUserId, categoryId;
			String note, clubNote, requested_start, rentSi, si;
			
			Spinner spinner = (Spinner) rootView.findViewById(R.id.spinnerRegNum);
			clubUserId = clubUsers.get(spinner.getSelectedItemPosition()).clubUserId;
			spinner = (Spinner) rootView.findViewById(R.id.spinnerCategory);
			categoryId = categories.get(spinner.getSelectedItemPosition()).categoryId;
			
			EditText editText = (EditText) rootView.findViewById(R.id.editTextSi);
			si = editText.getText().toString();			
			editText = (EditText) rootView.findViewById(R.id.editTextNote);
			note = editText.getText().toString();
			editText = (EditText) rootView.findViewById(R.id.editTextClubNote);
			clubNote = editText.getText().toString();
			editText = (EditText) rootView.findViewById(R.id.editTextRequestStat);
			requested_start = editText.getText().toString();
			
			CheckBox checkBox = (CheckBox) rootView.findViewById(R.id.checkBoxSi);
			if(checkBox.isChecked())
				rentSi = "1";
			else
				rentSi="";
			
			
			Log.i("LoginEvent", "clubuserid: "+Integer.toString(clubUserId));
			Log.i("LoginEvent", "category: "+Integer.toString(categoryId));
			
			// prihlaseni na zavod 2773. Id kategorie je unikatni pro dany zavod.
			orisAPI.createEntry("testOris", "testoris", Integer.toString(clubUserId), 
					Integer.toString(categoryId), si, note, clubNote, requested_start, rentSi, "", "");						
		} catch (Exception e) {
			Toast.makeText(this.getActivity().getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
		//Toast.makeText(getActivity().getApplicationContext(), "Stisknuto Přihlásit na závod", Toast.LENGTH_SHORT).show();
	}
	
	private void back(){
		getActivity().getFragmentManager().popBackStack();
	}
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		switch(parent.getId()) {
	        case R.id.spinnerRegNum:
				try {
					
					Log.i("onItemSlected", "clubUserId: "+clubUsers.get(position).clubUserId);
					Log.i("onItemSlected", "eventId: "+Integer.toString(eventId));
					// zjisteni platnych kategorii pro testovaciho uzivatele pro dany zavod. Vracene id kategorii jsou 
					// unikatni pro dany zacvod
					orisAPI.getValidClasses(Integer.toString(clubUsers.get(position).clubUserId), Integer.toString(eventId));						
				} catch (Exception e) {
					Toast.makeText(this.getActivity().getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
					e.printStackTrace();
				}
			break;
			
	        case R.id.spinnerCategory:
	        	
	        	Log.i("onItemSelected","fee: "+categories.get(position).fee);
	        	TextView textView = (TextView) rootView.findViewById(R.id.textViewFeeValue);
				textView.setText(categories.get(position).fee);
	        	break;
		}		
		Log.i("LoginEvent","vybran prvek: "+position);
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		
	}
	 
	@Override
	public void onDownloadComplete(JSONObject data, Method method) {
		
		switch(method){
		
			case GET_CLUB_USERS:
				try {
					// ulozim si vsechny registrace v kluvech a jejich id a vypisu do 
					//vysouvaciho seznamu
					JSONObject dataObj = data.getJSONObject("Data");
					Log.i("GET_CLUB_USERS",dataObj.toString());
					Iterator<String> keys= dataObj.keys();
					List<String> regNumList = new ArrayList<String>(); 
					String name = user.getFirstName()+" "+user.getLastName()+" - ";
					while(keys.hasNext()){
						String key = keys.next();											
						
						ClubUser clubUser = new ClubUser();
						clubUser.clubUserId = dataObj.getJSONObject(key).getInt("ID");
						clubUser.regNumber = dataObj.getJSONObject(key).getString("RegNo");
						
						regNumList.add(name+clubUser.regNumber);
						clubUsers.add(clubUser);
					}
					
					Spinner spinnerRegNum = (Spinner) rootView.findViewById(R.id.spinnerRegNum);					
					ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(),
						android.R.layout.simple_spinner_item, regNumList);
					dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnerRegNum.setAdapter(dataAdapter);
					
					TextView textView = (TextView) rootView.findViewById(R.id.textViewLicenceValue);
					textView.setText(user.getLicense());
				
				} catch (JSONException e) {					
					e.printStackTrace();
				}
				break;
			
			case GET_VALID_CLASSES:		
				try {	
					// zjisteni platnych kategorii pro testovaciho uzivatele pro dany zavod. Vracene id kategorii jsou 
					// unikatni pro dany zacvod
					JSONObject dataObj = data.getJSONObject("Data");
						
					Log.i("GET_VALID_CLASSES",dataObj.toString());
					Iterator<String> keys= dataObj.keys();
					List<String> categoryList = new ArrayList<String>(); 
					while(keys.hasNext()){
						String key = keys.next();											
						
						Category category = new Category();
						category.categoryId = dataObj.getJSONObject(key).getInt("ID");
						category.categoryName = dataObj.getJSONObject(key).getString("ClassDesc");
						category.fee = dataObj.getJSONObject(key).getString("Fee")+" Kč";
						
						categoryList.add(category.categoryName);
						categories.add(category);
					}
					
					Spinner spinnerCategories = (Spinner) rootView.findViewById(R.id.spinnerCategory);					
					ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(),
						android.R.layout.simple_spinner_item, categoryList);
					dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnerCategories.setAdapter(dataAdapter);
					
				} catch (JSONException e) {					
					e.printStackTrace();
				}									
				break;
			
			case CREATE_ENTRY:
				// zjistim jestli probehlo prihlaseni spravne, pokud ano ulozim si id prihlasky
				try {
					Log.i("CREATE_ENTRY",data.toString());
					String status =data.getString("Status");
					if(status.equals("OK")){
						JSONObject entryObj = data.getJSONObject("Data").getJSONObject("Entry");
						entryId = entryObj.getInt("ID");
						Log.i("LoginFragment", "entryID: "+ entryId);
						Toast.makeText(this.getActivity().getApplicationContext(), "Přihlášení proběhlo úspěšně.", Toast.LENGTH_SHORT).show();
						
						getActivity().getFragmentManager().popBackStack();
						getActivity().getFragmentManager().popBackStack();
					}
					else
						Toast.makeText(this.getActivity().getApplicationContext(), status, Toast.LENGTH_SHORT).show();	
				} catch (JSONException e) {					
					e.printStackTrace();					
				}
				
				break;
				
			case UNABLE_RETRIEVE_DATA:
				
				break;
					
			default:
				break;
		}	
	}
	
	protected class ClubUser{
		String regNumber;
		int clubUserId;
	}
	
	protected class Category{
		String categoryName;
		int categoryId;
		String fee;
	}

}
