package cz.vutbr.fit.tam.oris;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class OrisHelper extends SQLiteOpenHelper {
	
	private static final String DATABASE_NAME = "eventlist.db";
	private static final int SCHEMA_VERSION = 1;

	public OrisHelper(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	/**
	 * Vytvoří databázi
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		// Type boolean is like integer 0 - false, 1 - true
		db.beginTransaction();
		try {
			// Vytvoření tabulek v databázi
			db.execSQL("CREATE TABLE events (id INTEGER PRIMARY KEY, name TEXT, date STRING, org1 INTEGER, org2 INTEGER, " +
					"region TEXT, sport INTEGER, discipline INTEGER, level INTEGER, entryDate1 TEXT, entryDate2 TEXT, entryDate3 TEXT, " +
					"ranking INTEGER, cancelled INTEGER, GPSLat REAL, GPSLon REAL);");			
			db.execSQL("CREATE TABLE club (id INTEGER PRIMARY KEY, abbr TEXT, name TEXT);");
			db.execSQL("CREATE TABLE sport (id INTEGER PRIMARY KEY, nameCZ TEXT, nameEN TEXT);");
			db.execSQL("CREATE TABLE discipline (id INTEGER PRIMARY KEY, shortName TEXT, nameCZ TEXT, nameEN TEXT);");
			db.execSQL("CREATE TABLE level (id INTEGER PRIMARY KEY, shortName TEXT, nameCZ TEXT, nameEN TEXT);");
			db.execSQL("CREATE TABLE eventInfo (id INTEGER PRIMARY KEY, place TEXT, startTime STRING, eventOfficeCloses STRING,"+
					" rankingDecisionDate TEXT, map TEXT, services TEXT, entryDescCZ TEXT, entryBankAccount TEXT, entryInfo TEXT, director TEXT,  mainReferee TEXT, courseSetter_1 TEXT, sw TEXT, "
					+ "categories TEXT, entriesCount INTEGER);");
			db.execSQL("CREATE TABLE documents (id INTEGER PRIMARY KEY, url TEXT, nameCZ TEXT, otherDescCZ TEXT, eventId INTEGER, FOREIGN KEY(eventId) REFERENCES eventInfo(id));");
			db.execSQL("CREATE TABLE links (id INTEGER PRIMARY KEY, url TEXT, nameCZ TEXT, otherDescCZ TEXT, eventId INTEGER, FOREIGN KEY(eventId) REFERENCES eventInfo(id));");
			db.execSQL("CREATE TABLE classes (id INTEGER PRIMARY KEY, name TEXT, distance REAL, climbing INTEGER, controls INTEGER);");
			db.execSQL("CREATE TABLE results (id INTEGER PRIMARY KEY, name TEXT, classID INTEGER, club TEXT, sort INTEGER, place TEXT, time STRING, loss STRING,"
					+ " eventId INTEGER, FOREIGN KEY(eventId) REFERENCES eventInfo(id), FOREIGN KEY(classId) REFERENCES classes(id));");
			db.execSQL("CREATE TABLE entries (id INTEGER PRIMARY KEY, name TEXT, classID INTEGER, clubId INTEGER, si INTEGER, fee INTEGER, regNo STRING,  licence TEXT,"
					+ " rentSI INTEGER, eventId INTEGER, FOREIGN KEY(eventId) REFERENCES eventInfo(id), FOREIGN KEY(classId) REFERENCES classes(id), FOREIGN KEY(clubId) REFERENCES club(id));");
			db.setTransactionSuccessful();
		} catch (SQLException e) {
			Log.e("Error creating tables", e.toString());
			throw e;
		} finally {
			db.endTransaction();
		}
		
	}
	
	/**
	 * Upgrade databáze
	 * @param db SQLiteDatabase
	 * @param oldVersion Number of version in a device
	 * @param newVersion Number of actual version of database
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("upgrade", "Upgrading database from version " + oldVersion
				+ " to " + newVersion + ", which will destroy all old data.");

		db.beginTransaction();
		try {
			// Smazání tabulek
			db.execSQL("DROP TABLE IF EXISTS events;");
			db.execSQL("DROP TABLE IF EXISTS entries;");
			db.execSQL("DROP TABLE IF EXISTS club;");
			db.execSQL("DROP TABLE IF EXISTS sport;");
			db.execSQL("DROP TABLE IF EXISTS discipline;");
			db.execSQL("DROP TABLE IF EXISTS level;");
			db.execSQL("DROP TABLE IF EXISTS eventInfo;");
			db.execSQL("DROP TABLE IF EXISTS documents;");
			db.execSQL("DROP TABLE IF EXISTS links;");
			db.execSQL("DROP TABLE IF EXISTS results;");			
			db.execSQL("DROP TABLE IF EXISTS classes;");
			db.setTransactionSuccessful();
			
		} catch (SQLException e) {
			Log.e("Error upgrading tables", e.toString());
			throw e;
		} finally {
			db.endTransaction();
		}
		// Vytvoření celé databáze znovu
		onCreate(db);
	}
	
	/**
	 * Insert data to the table events.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param name
	 * @param date
	 * @param org1
	 * @param org2
	 * @param region
	 * @param sport
	 * @param discipline
	 * 
	 */
	public void insertEvent(int id, String name, String date, int org1, int org2, String reg, int sport, int discipline,
			int level, String entryDate1, String entryDate2, String entryDate3, int rank, int cancel, double lat, double lon) {
		// ContentValues pro vložení dat do databáze
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("name", name);
		cv.put("date", date);
		cv.put("org1", org1);
		cv.put("org2", org2);
		cv.put("region", reg);
		cv.put("sport", sport);
		cv.put("discipline", discipline);
		cv.put("level", level);
		cv.put("entryDate1", entryDate1);
		cv.put("entryDate2", entryDate2);
		cv.put("entryDate3", entryDate3);
		cv.put("ranking", rank);
		cv.put("cancelled", cancel);
		cv.put("GPSLat", lat);
		cv.put("GPSLon", lon);

		// Zapsání do databáze
		getWritableDatabase().insert("events", null, cv);
	}
	
	/**
	 * Aktualizace dat v tabulce events.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param name
	 * @param date
	 * @param org1
	 * @param org2
	 * @param region
	 * @param sport
	 * @param discipline
	 * 
	 */
	public void updateEvent(int id, String name, String date, int org1, int org2, String reg, int sport, int discipline,
			int level, String entryDate1, String entryDate2, String entryDate3, int rank, int cancel, double lat, double lon) {
		// ContentValues pro vložení dat do databáze
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("name", name);
		cv.put("date", date);
		cv.put("org1", org1);
		cv.put("org2", org2);
		cv.put("region", reg);
		cv.put("sport", sport);
		cv.put("discipline", discipline);
		cv.put("level", level);
		cv.put("entryDate1", entryDate1);
		cv.put("entryDate2", entryDate2);
		cv.put("entryDate3", entryDate3);
		cv.put("ranking", rank);
		cv.put("cancelled", cancel);
		cv.put("GPSLat", lat);
		cv.put("GPSLon", lon);

		// Zapsání do databáze
		getWritableDatabase().update("events", cv, "id="+id, null);
	}
	
	/**
	 * Vložení dat do tabulky eventInfo.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param startTime
	 * @param eventOfficeCloses
	 * @param rankingDecisionDate
	 * @param map
	 * @param services
	 * @param entryDescCZ
	 * @param entryBankAccount
	 * @param entryInfo
	 * @param director
	 * @param mainReferee
	 * @param courseSetter
	 * @param sw
	 * @param categories
	 * 
	 */
	public void insertEventInfo(int id, String place, String startTime, String eventOfficeCloses, String rankingDecisionDate, 
			String map, String services, String entryDescCZ, String entryBankAccount, String entryInfo, String director, 
			String mainReferee, String courseSetter, String sw, String categories, int entriesCount) {
		// ContentValues pro vložení dat do databáze
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("place", place);
		cv.put("startTime", startTime);
		cv.put("eventOfficeCloses", eventOfficeCloses);
		cv.put("rankingDecisionDate", rankingDecisionDate);
		cv.put("map", map);
		cv.put("services", services);
		cv.put("entryDescCZ", entryDescCZ);
		cv.put("entryBankAccount", entryBankAccount);
		cv.put("entryInfo", entryInfo);
		cv.put("director", director);
		cv.put("mainReferee", mainReferee);
		cv.put("courseSetter_1", courseSetter);
		cv.put("sw", sw);
		cv.put("categories", categories);	
		cv.put("entriesCount", entriesCount);	
		
		// Zapsání do databáze
		getWritableDatabase().insert("eventInfo", null, cv);
	}
	
	/**
	 * Aktualizace dat v tabulce eventInfo.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param startTime
	 * @param eventOfficeCloses
	 * @param rankingDecisionDate
	 * @param map
	 * @param services
	 * @param entryDescCZ
	 * @param entryBankAccount
	 * @param entryInfo
	 * @param director
	 * @param mainReferee
	 * @param courseSetter
	 * @param sw
	 * @param categories
	 * 
	 */
	public void updateEventInfo(int id, String place, String startTime, String eventOfficeCloses, String rankingDecisionDate, 
			String map, String services, String entryDescCZ, String entryBankAccount, String entryInfo, String director, 
			String mainReferee, String courseSetter, String sw, String categories, int entriesCount) {
		// ContentValues pro vložení dat do databáze
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("place", place);
		cv.put("startTime", startTime);
		cv.put("eventOfficeCloses", eventOfficeCloses);
		cv.put("rankingDecisionDate", rankingDecisionDate);
		cv.put("map", map);
		cv.put("services", services);
		cv.put("entryDescCZ", entryDescCZ);
		cv.put("entryBankAccount", entryBankAccount);
		cv.put("entryInfo", entryInfo);
		cv.put("director", director);
		cv.put("mainReferee", mainReferee);
		cv.put("courseSetter_1", courseSetter);
		cv.put("sw", sw);
		cv.put("categories", categories);	
		cv.put("entriesCount", entriesCount);	
		
		// Zapsání do databáze
		getWritableDatabase().update("eventInfo", cv, "id="+id, null);
	}
	
	/**
	 * Vložení dat do tabulky documents.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param url
	 * @param nameCZ
	 * @param otherDescCZ
	 * @param eventId	
	 * 
	 */
	public void insertDocuments(int id, String url, String nameCZ, String otherDescCZ, int eventId) {
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("url", url);
		cv.put("nameCZ", nameCZ);
		cv.put("otherDescCZ", otherDescCZ);
		cv.put("eventId", eventId);

		getWritableDatabase().insert("documents", null, cv);
	}
	
	/**
	 * Aktualizace dat v tabulce documents.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param url
	 * @param nameCZ
	 * @param otherDescCZ
	 * @param eventId	
	 * 
	 */
	public void updateDocuments(int id, String url, String nameCZ, String otherDescCZ, int eventId) {
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("url", url);
		cv.put("nameCZ", nameCZ);
		cv.put("otherDescCZ", otherDescCZ);
		cv.put("eventId", eventId);

		getWritableDatabase().update("documents", cv, "id="+id, null);
	}
	
	/**
	 * Vložení dat do tabulky links.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param url
	 * @param nameCZ
	 * @param otherDescCZ
	 * @param eventId
	 * 
	 */
	public void insertLinks(int id, String url, String nameCZ, String otherDescCZ, int eventId) {
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("url", url);
		cv.put("nameCZ", nameCZ);
		cv.put("otherDescCZ", otherDescCZ);
		cv.put("eventId", eventId);

		getWritableDatabase().insert("links", null, cv);
	}
	
	/**
	 * Aktualizace dat v tabulce links.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param url
	 * @param nameCZ
	 * @param otherDescCZ
	 * @param eventId
	 * 
	 */
	public void updateLinks(int id, String url, String nameCZ, String otherDescCZ, int eventId) {
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("url", url);
		cv.put("nameCZ", nameCZ);
		cv.put("otherDescCZ", otherDescCZ);
		cv.put("eventId", eventId);

		getWritableDatabase().update("links", cv, "id="+id, null);
	}
	
	/**
	 * Vložení dat do tabulky results.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param name
	 * @param classDesc
	 * @param club
	 * @param place
	 * @param time
	 * @param loss
	 * @param eventId
	 * 
	 */
	public void insertResult(int id, String name, int classId, String club, int sort, String place, String time, String loss, int eventId) {
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("name", name);
		cv.put("classId", classId);
		cv.put("club", club);
		cv.put("sort", sort);
		cv.put("place", place);
		cv.put("time", time);
		cv.put("loss", loss);
		cv.put("eventId", eventId);

		getWritableDatabase().insert("results", null, cv);
	}
	
	/**
	 * Aktualizace dat v tabulce results.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param name
	 * @param classDesc
	 * @param club
	 * @param place
	 * @param time
	 * @param loss
	 * @param eventId
	 * 
	 */
	public void updateResult(int id, String name, int classId, String club, int sort, String place, String time, String loss, int eventId) {
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("name", name);
		cv.put("classId", classId);
		cv.put("club", club);
		cv.put("sort", sort);
		cv.put("place", place);
		cv.put("time", time);
		cv.put("loss", loss);
		cv.put("eventId", eventId);

		getWritableDatabase().update("results", cv, "id="+id, null);
	}
	
	/**
	 * Vložení dat do tabulky entries.
	 * Parametry jsou hodnoty v tabulce.
	 * @param entryId
	 * @param name
	 * @param classId
	 * @param clubId
	 * @param si
	 * @param fee
	 * @param regNo
	 * @param licence
	 * @param eventId
	 * 
	 */
	public void insertEntry(int entryId, String name, int classId, int clubId,
			int si, int fee, String regNo, String licence, int rentSI, int eventId) {

		ContentValues cv = new ContentValues();

		cv.put("id", entryId);
		cv.put("name", name);
		cv.put("classId", classId);
		cv.put("clubId", clubId);
		cv.put("si", si);
		cv.put("fee", fee);
		cv.put("regNo", regNo);
		cv.put("licence", licence);
		cv.put("rentSI", rentSI);
		cv.put("eventId", eventId);

		getWritableDatabase().insert("entries", null, cv);		
	}
	
	/**
	 * Aktualizace dat v tabulce entries.
	 * Parametry jsou hodnoty v tabulce.
	 * @param entryId
	 * @param name
	 * @param classId
	 * @param clubId
	 * @param si
	 * @param fee
	 * @param regNo
	 * @param licence
	 * @param eventId
	 * 
	 */
	public void updateEntry(int entryId, String name, int classId, int clubId,
			int si, int fee, String regNo, String licence, int rentSI, int eventId) {

		ContentValues cv = new ContentValues();

		cv.put("id", entryId);
		cv.put("name", name);
		cv.put("classId", classId);
		cv.put("clubId", clubId);
		cv.put("si", si);
		cv.put("fee", fee);
		cv.put("regNo", regNo);
		cv.put("licence", licence);
		cv.put("rentSI", rentSI);
		cv.put("eventId", eventId);

		getWritableDatabase().update("entries", cv, "id="+entryId, null);		
	}
	
	/**
	 * Smazani dat v tabulce entries.	
	 * @param eventId
	 * 
	 */
	public void deleteEventEntries(int eventId) {
		getWritableDatabase().delete("entries", "eventId="+eventId, null);
	}
	
	/**
	 * Vložení dat do tabulky classes.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param name
	 * @param distance
	 * @param climbing
	 * @param controls	
	 * 
	 */
	public void insertClass(int id, String name, double distance, int climbing, int controls) {
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("name", name);
		cv.put("distance", distance);
		cv.put("climbing", climbing);
		cv.put("controls", controls);
		
		getWritableDatabase().insert("classes", null, cv);
	}
	
	/**
	 * Aktualizace dat v tabulce classes.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param name
	 * @param distance
	 * @param climbing
	 * @param controls	
	 * 
	 */
	public void updateClass(int id, String name, double distance, int climbing, int controls) {
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("name", name);
		cv.put("distance", distance);
		cv.put("climbing", climbing);
		cv.put("controls", controls);

		getWritableDatabase().update("classes", cv, "id="+id, null);
	}
	
	/**
	 * Vložení dat do tabulky sport.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param nameCZ
	 * @param nameEN
	 */
	public void insertSport(int id, String nameCZ, String nameEN) {
		ContentValues cv = new ContentValues();
		
		cv.put("id", id);
		cv.put("nameCZ", nameCZ);
		cv.put("nameEN", nameEN);

		getWritableDatabase().insert("sport", null, cv);
	}
	
	/**
	 * Vložení dat do tabulky discipline.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param name shortName
	 * @param nameCZ
	 * @param nameEN
	 */
	public void insertDiscipline(int id, String name, String nameCZ, String nameEN) {
		ContentValues cv = new ContentValues();
		
		cv.put("id", id);
		cv.put("shortName", name);
		cv.put("nameCZ", nameCZ);
		cv.put("nameEN", nameEN);

		getWritableDatabase().insert("discipline", null, cv);
	}
	
	/**
	 * Vložení dat do tabulky level.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param name shortName
	 * @param nameCZ
	 * @param nameEN
	 */
	public void insertLevel(int id, String name, String nameCZ, String nameEN) {
		ContentValues cv = new ContentValues();
		
		cv.put("id", id);
		cv.put("shortName", name);
		cv.put("nameCZ", nameCZ);
		cv.put("nameEN", nameEN);

		getWritableDatabase().insert("level", null, cv);
	}
	
	/**
	 * Vložení dat do tabulky club.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param abbr
	 * @param name
	 */
	public void insertClub(int id, String abbr, String name) {
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("abbr", abbr);
		cv.put("name", name);

		getWritableDatabase().insert("club", null, cv);
	}
	
	/**
	 * Aktualizace dat v tabulce club.
	 * Parametry jsou hodnoty v tabulce.
	 * @param id
	 * @param abbr
	 * @param name
	 */
	public void updateClub(int id, String abbr, String name) {
		ContentValues cv = new ContentValues();

		cv.put("id", id);
		cv.put("abbr", abbr);
		cv.put("name", name);

		getWritableDatabase().update("club", cv, "id="+id, null);
	}
	
	/**
	 * Vrací počet záznamů v dané tabulce.
	 * @param table Název tabulky
	 * @return Počet záznamů v tabulce
	 */
	public int tableCount(String table) {
		Cursor c = getReadableDatabase().rawQuery("SELECT id FROM " + table + " ORDER BY id", null);
		int count = c.getCount();
		c.close();
		return count;
	}
	
	/**
	 * Existence záznamu s daným ID v dané tabulce
	 * @param table Název tabulky
	 * @param id ID záznamu v tabulce
	 * @return true - záznam existuje; false - záznam neexistuje
	 */
	public boolean recordExists(String table, int id) {
		String[] args = {Integer.toString(id)};
		Cursor c = getReadableDatabase().rawQuery("SELECT id FROM " + table + " WHERE id=?", args);
		int count = c.getCount();
		c.close();
		if (count == 0)
			return false;
		else
			return true;
	}
	
	/**
	 * Vrátí hodnotu String z daného sloupce z dané tabulky
	 * @param table Název tabulky
	 * @param column Název sloupce
	 * @param id ID záznamu v tabulce
	 * @return Hledaný záznam
	 */
	public String getStringValue(String table, String column, int id) {
		String _id = String.valueOf(id);
		String[] args = {_id};
		Cursor c = getReadableDatabase().rawQuery(
				"SELECT id, " + column + " FROM " + table + " WHERE id=?", args);
		c.moveToFirst();
		if (c.getCount() == 0) {
			return null;
		}
		int index = c.getColumnIndex(column);
		String value = c.getString(index);
		c.close();
		return value;
	}
	
	/**
	 * Vrací kurzor se záznamy z tabulky events.
	 * @param from Datum od
	 * @param to Datum to
	 * @param orderBy Seřazené podle (název sloupce)
	 * @param sport Jaký sport
	 * @return Cursor
	 */
	public Cursor getEvents(String from, String to, String orderBy, int sport) {
		String[] args = {from, to};
		Cursor c = null;
		if (sport == 0) {
			c = getReadableDatabase().rawQuery("SELECT id, name, date, org1, region, sport, discipline, level, GPSLat, GPSLon FROM events "
					+ "WHERE date>=? and date<=? ORDER BY " + orderBy, args);
		} else {
			c = getReadableDatabase().rawQuery("SELECT id, name, date, org1, region, sport, discipline, level, GPSLat, GPSLon FROM events "
					+ "WHERE date>=? and date<=? and sport=" + sport + " ORDER BY " + orderBy, args);
		}

		int count = c.getCount();
		Log.i("OrisHelper", String.valueOf(count));
		return c;
	}

	/**
	 * Vrací kurzor se záznamy z tabulky events podle regionu.
	 * @param from Datum od
	 * @param to Datum to
	 * @param orderBy Seřazené podle (název sloupce)
	 * @param region Požadovaný region
	 * @param sport Jaký sport
	 * @return Cursor
	 */
	public Cursor getEventsRegion(String from, String to, String orderBy, String region, int sport) {
		String[] args = {from, to, region};
		Cursor c = null;
		if (sport == 0) {
			c = getReadableDatabase().rawQuery("SELECT id, name, date, org1, region, sport, discipline, level, GPSLat, GPSLon FROM events "
				+ "WHERE date>=? and date<=? and region=? ORDER BY " + orderBy, args);
		} else {
			c = getReadableDatabase().rawQuery("SELECT id, name, date, org1, region, sport, discipline, level, GPSLat, GPSLon FROM events "
					+ "WHERE date>=? and date<=? and region=? and sport=" + sport + " ORDER BY " + orderBy, args);
		}
		int count = c.getCount();
		Log.i("OrisHelper", String.valueOf(count));
		return c;
	}
	
	/**
	 * Vrací počet závodů zvoleného sportu.
	 * @param from Datum od
	 * @param to Datum do
	 * @param sport Jaký sport
	 * @param region Jaký region
	 * @return
	 */
	public int getEventsCountSport(String from, String to, int sport, String region) {
		String[] args = {from, to, String.valueOf(sport), region};
		Cursor c = null;
		if (region == "Vše") {
			Log.d("OrisHelper", "vše");
			c = getReadableDatabase().rawQuery("SELECT id, name, date, org1, region, sport, discipline, level FROM events "
				+ "WHERE date>=? and date<=? and sport=1", args);
		} else {
			c = getReadableDatabase().rawQuery("SELECT id, name, date, org1, region, sport, discipline, level FROM events "
					+ "WHERE date>=? and date<=? and sport=? and region=?", args);
		}
		
		int count = c.getCount();
		c.close();
		return count;
	}

	/**
	 * Vrací počet závodů v daném časovém rozmezí
	 * @param from Datum od
	 * @param to Datum do
	 * @return Počet závodů v daném časovém rozmezí
	 */
	public int getEventsCount(String from, String to) {
		String[] args = {from,to};
		Cursor c = getReadableDatabase().rawQuery("SELECT id, name, date, org1, region, sport, discipline, level "
				+ "FROM events WHERE date>=? and date<=?", args);		
		int count = c.getCount();
		return count;
	}
	
	/**
	 * Vraci kurzor se zaznamem z tabulky eventInfo.
	 * @param id Id zavodu
	 * @return Cursor
	 */
	public Cursor getEventInfo(int id) {
		String[] args = {Integer.toString(id)};
		Cursor c = getReadableDatabase().rawQuery("SELECT * FROM eventInfo WHERE id=?",args);		
		return c;
	}
	
	/**
	 * Vraci kurzor se zaznamem z tabulky event.
	 * @param id Id zavodu
	 * @return Cursor
	 */
	public Cursor getEvent(int id) {
		String[] args = {Integer.toString(id)};
		Cursor c = getReadableDatabase().rawQuery("SELECT * FROM events WHERE id=?",args);		
		return c;
	}
	
	/**
	 * Vraci kurzor se zaznamem z tabulky eventDocuments.
	 * Dokumenty k danemu zavodu
	 * @param eventId Id zavodu
	 * @return Cursor
	 */
	public Cursor getEventDocuments(int eventId) {
		String[] args = {Integer.toString(eventId)};
		Cursor c = getReadableDatabase().rawQuery("SELECT * FROM documents WHERE eventId=?",args);		
		return c;
	}
	
	/**
	 * Vraci kurzor se zaznamem z tabulky eventLinks.
	 * Odkazy k danemu zavodu
	 * @param eventId Id zavodu
	 * @return Cursor
	 */
	public Cursor getEventLinks(int eventId) {
		String[] args = {Integer.toString(eventId)};
		Cursor c = getReadableDatabase().rawQuery("SELECT * FROM links WHERE eventId=?",args);		
		return c;
	}
	
	/**
	 * Vraci pocet vysledku daneho zavodu.
	 * @param eventId Id zavodu
	 * @return pocet vysledku
	 */
	public int getResultsCount(int eventId) {
		String[] args = {Integer.toString(eventId)};
		Cursor c = getReadableDatabase().rawQuery("SELECT * FROM results WHERE eventId=?",args);		
		int count = c.getCount();
		return count;		
	}
	
	/**
	 * Vraci kurzor s vysledky daneho zavodu a dane kategorie.
	 * @param eventId Id zavodu
	 * @param classId kategorie
	 * @return Cursor
	 */
	public Cursor getEventResults(int eventId, int classId) {
		String[] args = {Integer.toString(eventId), Integer.toString(classId)};
		Cursor c = getReadableDatabase().rawQuery("SELECT * FROM results WHERE eventId=? AND classId=? ORDER BY sort",args);		
		return c;
	}

	/**
	 * Vraci kurzor zaznam z tabulky classes.
	 * @param name Nazev kategorie
	 * @return Cursor
	 */
	public Cursor getClass(String name) {
		String[] args = {name};
		Cursor c = getReadableDatabase().rawQuery("SELECT * FROM classes WHERE name=?",args);		
		return c;
	}
	
	/**
	 * Vraci zkratku klubu daneho klubu podle nazvu klubu.
	 * @param clubName nazev klubu
	 * @return zkratka klubu
	 */
	public String getClubAbbr(String clubName) {
		String abbr = null;
		String[] args = {clubName};
		Cursor c = getReadableDatabase().rawQuery("SELECT abbr, name FROM club WHERE name=?", args);
		c.moveToFirst();
		if(c.getCount() != 0){
			abbr = c.getString(0);				
		}
		else
			abbr = clubName;
		
		c.close();
		return abbr;
	}
	
	/**
	 * Vraci kurzor s danym klubem.
	 * @param id Id zavodu
	 * @return Cursor
	 */
	public Cursor getClub(int id) {
		String[] args = {Integer.toString(id)};
		Cursor c = getReadableDatabase().rawQuery("SELECT abbr, name FROM club WHERE id=?", args);
		return c;
	}
	
	/**
	 * Vraci pocet prihlasek daneho zavodu.
	 * @param eventId Id zavodu
	 * @return pocet prihlasek
	 */
	public int getEntriesCount(int eventId) {
		String[] args = {Integer.toString(eventId)};
		Cursor c = getReadableDatabase().rawQuery("SELECT * FROM entries WHERE eventId=?",args);		
		int count = c.getCount();
		return count;		
	}
	
	/**
	 * Vraci kurzor s prihlasky daneho zavodu a dane kategorie.
	 * @param eventId Id zavodu
	 * @param classId kategorie
	 * @return Cursor
	 */
	public Cursor getEventEntries(int eventId, int classId) {
		String[] args = {Integer.toString(eventId), Integer.toString(classId)};
		Cursor c = getReadableDatabase().rawQuery("SELECT * FROM entries WHERE eventId=? AND classId=? ORDER BY regNo",args);		
		return c;
	}
	
	/**
	 * Vraci kurzor s danou prihlaskou pro dany zavod
	 * @param eventId Id zavodu
	 * @param regNumber kategorie
	 * @return Cursor
	 */
	public Cursor getEventEntry(int eventId, String regNumber) {
		String[] args = {Integer.toString(eventId), regNumber};
		Cursor c = getReadableDatabase().rawQuery("SELECT * FROM entries WHERE eventId=? AND regNo=?",args);		
		return c;
	}
}
