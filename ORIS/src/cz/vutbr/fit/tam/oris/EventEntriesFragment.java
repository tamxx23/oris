package cz.vutbr.fit.tam.oris;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import cz.vutbr.fit.tam.oris.OrisAPI.Method;
import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/** 
 * Fragment pro zobrazeni prihlasek na dany zavod.
 * Odvozena trida od BaseListFragment. Musi prespsat abstraktni metody:
 *     onDownloadComplete(JSONObject data, Method method) - stazeni prihlasek ze serveru do DB
 *     getDBList(int classId) - ziskani prihlasek z DB
 *     addHead() - vlozeni hlavicky do tabulky
 *     addRow(Cursor entryList) - vlozeni radku do tabulky
 *     setTitle() - nastaveni titulku actionbaru
 * @see BaseListFragment
 */
public class EventEntriesFragment extends BaseListFragment {	
	
	/** Priznak pro stahnuti aktualnich dat ze serveru */
	private boolean refresh = false;
	
	/** 
	 * Metoda onCreateView se vola pri vytvoreni fragmentu.
	 * Vyplni layout daty (pripadne pozada o data server).
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return view fragmentu
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {		
		super.onCreateView(inflater, container, savedInstanceState);		
		mainView = inflater.inflate(R.layout.fragment_event_results,container, false);							
		
		if (helper.getEntriesCount(eventId) == 0 || refresh) {
			try {
				helper.deleteEventEntries(eventId);
				this.api.getEventEntries(Integer.toString(eventId),"","","","","");
			} catch (Exception e) {
				Toast.makeText(this.getActivity().getApplicationContext(), "Nemáš připojení k INTERNETu", Toast.LENGTH_SHORT).show();
			}
			refresh = false; 
        } else { 
        	getDataFromDatabase();
        	displayList();	
        }
		
		return mainView;
	}	
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

	/** 
	 * Nastaveni zobrazeni moznosti v actionbaru
	 */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_filter).setVisible(false);
        menu.findItem(R.id.action_order_by).setVisible(false);
        menu.findItem(R.id.action_order).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }
    
    /** 
	 * Akce pri pouziti tlacitka na v actionbaru
	 */
    @SuppressLint("NewApi")
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {      
      
       // Handle action buttons
       switch(item.getItemId()) {
       case R.id.action_refresh:
    	 
    	  // Log.i("EventEntriesFragment", "action_refresh");
    	   refresh = true;
    	   FragmentManager fragmentManager = getFragmentManager();
    	   FragmentTransaction ft = fragmentManager.beginTransaction();
    	   ft.detach(this);
    	   ft.attach(this);
    	   ft.commit();
    	   //Log.i("EventEntriesFragment", "action_refresh DONE");
    	   return true;
       default:
    	   return super.onOptionsItemSelected(item);
       }
       
    }
	
    /** 
     * Metoda nastavi titulek actionbaru
	 */
	@Override
	protected void setTitle() {
		Cursor event = this.helper.getEvent(eventId);	
		event.moveToFirst();
		getActivity().setTitle(event.getString(1)+" - přihlášky");
		event.close();		
	}
	
	/** 
     * Metoda ziska z databaze prihlasky pro danou kategorii a zavod.
     * @param classId Id kategorie
     * @return Kurzor ziskane prihlasky
	 */
	@Override
	protected Cursor getDBList(int classId) {
		return this.helper.getEventEntries(this.eventId, classId);
	}
	
	/** 
     * Metoda vytvori hlavicku tabulky.
     * Je to prvni polozka v seznamu table v ClassTable.
     * @return Hlavicka tabulky (seznam stringu)
     * @see ClassTable
     * @see BaseListFragment
	 */
	@Override
	protected List<String> addHead(){
		List<String> row = new ArrayList<String>();
		row.add(getActivity().getString(R.string.regNumber));
		row.add(getActivity().getString(R.string.name));
		row.add(getActivity().getString(R.string.club));
		row.add(getActivity().getString(R.string.license));
		row.add(getActivity().getString(R.string.si));
		row.add(getActivity().getString(R.string.fee));	
		return row;
	}
	
	/** 
     * Metoda vytvori radek tabulky.
     * @param entryList Kurzor na prihlasku
     * @return Radek tabulky (seznam stringu)
	 */
	@Override
	protected List<String> addRow(Cursor entryList){
		List<String> row = new ArrayList<String>();						
		row.add(entryList.getString(6));
		row.add(entryList.getString(1));			
		
		Cursor clubC = this.helper.getClub(entryList.getInt(3));
		clubC.moveToFirst();
		if(clubC.getCount() != 0)
			row.add(clubC.getString(0));
		else
			row.add("");
		row.add(entryList.getString(7));
		if(entryList.getInt(4) != 0)
			row.add(Integer.toString(entryList.getInt(4)));
		else
			row.add("");
		row.add(Integer.toString(entryList.getInt(5))+ " Kč");
		clubC.close();
		return row;
	}

	/** 
	 * Metoda z OrisAPIListener, ktera se asynchrone vola z OrisAPI se stazenymi daty.
	 * Nacte prihlasky do databaze a pote vyplni layout
     * @param data Data z weboveho API jako JSONObject
     * @param method Typ dotazu (OrisAPI.Method)
     */
	@Override
	public void onDownloadComplete(JSONObject data, Method method) {
		Log.i("EventFragment", "onDownloadComplete");
		
		switch(method){
			case GET_EVENT_ENTRIES:				
				try {
					// nacteni prihlasek zavodu do databaze
					if(data.has("Data") && (data.optJSONObject("Data") != null)){
						JSONObject dataObj = data.getJSONObject("Data");
						Iterator<String> keys = dataObj.keys();
						Log.i("EventEntriesFragment",""+dataObj.length());										
						while(keys.hasNext()){
							int entryId=0, classId=0, si=0, clubId=0, fee=0, rentSI=0;
							String name = "", regNo="", licence=""; 
							String key = keys.next();
							if(dataObj.getJSONObject(key).has("ID") && (dataObj.getJSONObject(key).optInt("ID") != 0))
								entryId = dataObj.getJSONObject(key).getInt("ID");
							if(dataObj.getJSONObject(key).has("Name"))
								name = dataObj.getJSONObject(key).getString("Name");
							if(dataObj.getJSONObject(key).has("ClassID") && (dataObj.getJSONObject(key).optInt("ClassID") != 0))
								classId = dataObj.getJSONObject(key).getInt("ClassID");
							if(dataObj.getJSONObject(key).has("ClubID") && (dataObj.getJSONObject(key).optInt("ClubID") != 0))
								clubId = dataObj.getJSONObject(key).getInt("ClubID");
							if(dataObj.getJSONObject(key).has("SI") && (dataObj.getJSONObject(key).optInt("SI") != 0))
								si = dataObj.getJSONObject(key).getInt("SI");
							if(dataObj.getJSONObject(key).has("Fee") && (dataObj.getJSONObject(key).optInt("Fee") != 0))
								fee = dataObj.getJSONObject(key).getInt("Fee");
							if(dataObj.getJSONObject(key).has("RegNo"))
								regNo = dataObj.getJSONObject(key).getString("RegNo");
							if(dataObj.getJSONObject(key).has("Licence") && (dataObj.getJSONObject(key).getString("Licence") != "null"))
								licence = dataObj.getJSONObject(key).getString("Licence");
							if(dataObj.getJSONObject(key).has("RentSI") && (dataObj.getJSONObject(key).optInt("Fee") != 0))
								rentSI = dataObj.getJSONObject(key).getInt("RentSI");
							
							if (!(this.helper.recordExists("entries", entryId))) {
								this.helper.insertEntry(entryId, name, classId, clubId, si, fee, regNo, licence, rentSI, eventId);							
							}
							else{
								this.helper.updateEntry(entryId, name, classId, clubId, si, fee, regNo, licence, rentSI, eventId);	
							}
						}	
					}					
					
				} catch (JSONException e) {					
					e.printStackTrace();
				}		
				
				//vlozeni do databaze
				getDataFromDatabase();
	        	//zobrazeni prihlasek	        	
	        	displayList();
	        	
				break;
				
			case UNABLE_RETRIEVE_DATA:
				
			break;
				
			default:
				break;
		}
		
	}
}
