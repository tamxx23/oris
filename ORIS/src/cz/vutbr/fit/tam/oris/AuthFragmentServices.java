package cz.vutbr.fit.tam.oris;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

public class AuthFragmentServices extends BaseAuthFragment{

	@Override
	protected void nextFragment() {
		ServicesFragment services = new ServicesFragment();					
		Bundle bundle = new Bundle();
		bundle.putInt("id", eventId);						
		services.setArguments(bundle);		
		
		FragmentManager fragmentManager = getActivity().getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.content_frame, services);		
		transaction.addToBackStack(null);
		transaction.commit();
		
	}

}
