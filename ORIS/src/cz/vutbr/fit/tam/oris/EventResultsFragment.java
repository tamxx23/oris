package cz.vutbr.fit.tam.oris;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import cz.vutbr.fit.tam.oris.OrisAPI.Method;

/** 
 * Fragment pro zobrazeni vysledku po dany zavod.
 * Odvozena trida od BaseListFragment. Musi prespsat abstraktni metody:
 *     onDownloadComplete(JSONObject data, Method method) - stazeni vysledku ze serveru do DB
 *     getDBList(int classId) - ziskani vysledku z DB
 *     addHead() - vlozeni hlavicky do tabulky
 *     addRow(Cursor entryList) - vlozeni radku do tabulky
 *     setTitle() - nastaveni titulku actionbaru
 * @see BaseListFragment
 */
public class EventResultsFragment extends BaseListFragment {

	/** 
	 * Metoda onCreateView se vola pri vytvoreni fragmentu.
	 * Vyplni layout daty (pripadne pozada o data server).
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return view fragmentu
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);		
		
		mainView = inflater.inflate(R.layout.fragment_event_results,container, false);	
		
		if (helper.getResultsCount(eventId) == 0) {
			try {
				this.api.getEventResults(Integer.toString(eventId),"","","");
			} catch (Exception e) {
				Toast.makeText(this.getActivity().getApplicationContext(), "Nemáš připojení k INTERNETu", Toast.LENGTH_SHORT).show();
			}
				 
        } else { 
        	getDataFromDatabase();
        	displayList();	
        }
		
		return mainView;
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

	/** 
	 * Nastaveni zobrazeni moznosti v actionbaru
	 */	
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
    	menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_filter).setVisible(false);
        menu.findItem(R.id.action_order_by).setVisible(false);
        menu.findItem(R.id.action_order).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }
	
    /** 
     * Metoda nastavi titulek actionbaru
	 */
	@Override
	protected void setTitle() {
		Cursor event = this.helper.getEvent(eventId);	
		event.moveToFirst();
		getActivity().setTitle(event.getString(1)+" - výsledky");
		event.close();
	}
	
	/** 
     * Metoda ziska z databaze vysledky pro danou kategorii a zavod.
     * @param classId Id kategorie
     * @return Kurzor naziskane vysledky z DB
	 */
	@Override
	protected Cursor getDBList(int classId) {
		return this.helper.getEventResults(this.eventId, classId);
	}
	
	/** 
     * Metoda vytvori hlavicku tabulky.
     * Je to prvni polozka v seznamu table v ClassTable.
     * @return Hlavicka tabulky (seznam stringu)
     * @see ClassTable
     * @see BaseListFragment
	 */
	@Override
	protected List<String> addHead() {
		List<String> row = new ArrayList<String>();		
		row.add("");
		row.add(getActivity().getString(R.string.name));
		row.add(getActivity().getString(R.string.club));
		row.add(getActivity().getString(R.string.time));
		row.add(getActivity().getString(R.string.loss));
		return row;
	}
	
	/** 
     * Metoda vytvori radek tabulky.
     * @param entryList Kurzor na vysledek
     * @return Radek tabulky (seznam stringu)
	 */
	@Override
	protected List<String> addRow(Cursor list) {
		List<String> row = new ArrayList<String>();
		row.add(list.getString(5));
		row.add(list.getString(1));	
		//entry.club = list.getString(3);
		row.add(this.helper.getClubAbbr(list.getString(3)));						
		row.add(list.getString(6));
		row.add(list.getString(7));		
		return row;
	}

	/** 
	 * Metoda z OrisAPIListener, ktera se asynchrone vola z OrisAPI se stazenymi daty.
	 * Nacte vysledky do databaze a pote vyplni layout
     * @param data Data z weboveho API jako JSONObject
     * @param method Typ dotazu (OrisAPI.Method)
     */
	@Override
	public void onDownloadComplete(JSONObject data, Method method) {
		Log.i("EventFragment", "onDownloadComplete");
		
		switch(method){
			case GET_EVENT_RESULTS:				
				try {
					// nacteni vysledku zavodu do databaze
					if(data.has("Data") && (data.optJSONObject("Data") != null)){
						JSONObject dataObj = data.getJSONObject("Data");
						Iterator<String> keys = dataObj.keys();
						Log.i("EventResultFragment",""+dataObj.length());						
						while(keys.hasNext()){
							int id=0, sort=0, classId=0;
							String name = "", club="", place="", time="", loss=""; 
							String key = keys.next();
							if(dataObj.getJSONObject(key).has("ID"))
								id = dataObj.getJSONObject(key).getInt("ID");
							if(dataObj.getJSONObject(key).has("Name"))
								name = dataObj.getJSONObject(key).getString("Name");
							if(dataObj.getJSONObject(key).has("ClassID"))
								classId = dataObj.getJSONObject(key).getInt("ClassID");
							if(dataObj.getJSONObject(key).has("ClubNameResults"))
								club = dataObj.getJSONObject(key).getString("ClubNameResults");
							if(dataObj.getJSONObject(key).has("Sort"))
								sort = dataObj.getJSONObject(key).getInt("Sort");
							if(dataObj.getJSONObject(key).has("Place"))
								place = dataObj.getJSONObject(key).getString("Place");
							if(dataObj.getJSONObject(key).has("Time"))
								time = dataObj.getJSONObject(key).getString("Time");
							if(dataObj.getJSONObject(key).has("Loss"))
								loss = dataObj.getJSONObject(key).getString("Loss");
							
							if (!(this.helper.recordExists("results", id))) {
								this.helper.insertResult(id, name, classId, club, sort, place, time, loss, eventId);							
							}else{
								this.helper.updateResult(id, name, classId, club, sort, place, time, loss, eventId);
							}
						}	
					}					
					
				} catch (JSONException e) {					
					e.printStackTrace();
				}		
				
				//vlozeni do databaze
				getDataFromDatabase();
				
	        	// zobrazeni vysledku zavodu	        	
	        	displayList();
	        	
				break;
				
			case UNABLE_RETRIEVE_DATA:
				
			break;
				
			default:
				break;
		}
		
	}
}
