package cz.vutbr.fit.tam.oris;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import cz.vutbr.fit.tam.oris.OrisAPI.Method;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/** 
 * Fragment pro detail zavodu.
 * Načte informace z databaze (tabulka eventInfo) a naplni layout daty. 
 * V pripade, ze data nejsou v DB, tak se nejprve stahnou ze serveru ORIS API.
 */
public class EventFragment extends Fragment implements OrisAPIListener{

	/** Pristup do databaze */
	private OrisHelper helper;
	/** Layout fragmentu */
	private View rootView;
	/** id zavodu */
	private int eventId;
	/** Priznak pro stahnuti aktualnich dat ze serveru */
	private boolean refresh = false;
	
	/** 
	 * Metoda onCreateView se vola pri vytvoreni fragmentu.
	 * Vyplni layout daty (pripadne pozada o data server). Vytvari listenery pro tlacitka
	 * a zarovnani textu.
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return view fragmentu
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		rootView = inflater.inflate(R.layout.fragment_event,container, false);
		Bundle bundle = this.getArguments();
				
		eventId = bundle.getInt("id");
		OrisAPI orisAPI = new OrisAPI(this.getActivity(), this);
		this.helper = new OrisHelper(this.getActivity());
		
		if (helper.getEventInfo(eventId).getCount() == 0 || refresh) {
			// data zavodu nejsou v DB -> nacteni dat ze serveru do databaze
			try {				
				orisAPI.getEvent(Integer.toString(eventId));
			
			} catch (Exception e) {
				Toast.makeText(this.getActivity().getApplicationContext(), "Nemáš připojení k INTERNETu", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}
			refresh = false;
	     }else{
	    	 // data jsou v DB vyplni se layout
	    	 fillLayout();
	    	
	     }
		
		// listener na tlacitko "Vysledky"
		Button button= (Button) rootView.findViewById(R.id.buttonResults);
		button.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {		    	
		        showResults();
		    }
			
		});

		// listener na tlacitko "Prihlasky"
		button= (Button) rootView.findViewById(R.id.buttonEntries);
		button.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {		    	
		        showEntries();
		    }		
			
		});
		
		// listener na tlacitko "Prihlasit na zavod"
		button= (Button) rootView.findViewById(R.id.buttonLogin);
		button.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {		    	
		        login();
		    }
			
		});

		// listener na tlacitko "Odhlasit ze zavodu"
		button= (Button) rootView.findViewById(R.id.buttonLogout);
		button.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {		    	
		        logout();
		    }		
			
		});
		
		// listener na tlacitko "Doplnkove sluzby"
		button= (Button) rootView.findViewById(R.id.buttonServicesOrder);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {		    	
				servicesOrder();
				}	
			});		
		
		// listener, ktery se spusti po vytvoreni layoutu. Najde nejdelsi popisek v danem lineatLayout
		// a odsadi hodnoty od popisku
		final TextView titleRankingDecisionDate = (TextView)rootView.findViewById(R.id.titleRankingDecisionDate);
		ViewTreeObserver vto = titleRankingDecisionDate.getViewTreeObserver();
		vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
		    
			@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
			@Override
		    public void onGlobalLayout() {
		        
		        int maxWidth = rootView.findViewById(R.id.titleRankingDecisionDate).getWidth();
			    if(maxWidth < rootView.findViewById(R.id.titleEventName).getWidth())
					maxWidth = rootView.findViewById(R.id.titleEventName).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleDate).getWidth())
					maxWidth = rootView.findViewById(R.id.titleDate).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titlePlace).getWidth())
					maxWidth = rootView.findViewById(R.id.titlePlace).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleStartTime).getWidth())
					maxWidth = rootView.findViewById(R.id.titleStartTime).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleEventOfficeCloses).getWidth())
					maxWidth = rootView.findViewById(R.id.titleEventOfficeCloses).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleOrganizer).getWidth())
					maxWidth = rootView.findViewById(R.id.titleOrganizer).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleSport).getWidth())
					maxWidth = rootView.findViewById(R.id.titleSport).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleLevel).getWidth())
					maxWidth = rootView.findViewById(R.id.titleLevel).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleRegions).getWidth())
					maxWidth = rootView.findViewById(R.id.titleRegions).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleDiscipline).getWidth())
					maxWidth = rootView.findViewById(R.id.titleDiscipline).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleRanking).getWidth())
					maxWidth = rootView.findViewById(R.id.titleRanking).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleMap).getWidth())
					maxWidth = rootView.findViewById(R.id.titleMap).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleCoordinates).getWidth())
					maxWidth = rootView.findViewById(R.id.titleCoordinates).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleEntriesCount).getWidth())
					maxWidth = rootView.findViewById(R.id.titleEntriesCount).getWidth();
					        
				initTextView(R.id.titleRankingDecisionDate, maxWidth, 5);		
				initTextView(R.id.titleEventName, maxWidth, 5);
				initTextView(R.id.titleDate, maxWidth, 5);
				initTextView(R.id.titlePlace, maxWidth, 5);
				initTextView(R.id.titleStartTime, maxWidth, 5);
				initTextView(R.id.titleEventOfficeCloses, maxWidth, 5);
				initTextView(R.id.titleOrganizer, maxWidth, 5);
				initTextView(R.id.titleSport, maxWidth, 5);
				initTextView(R.id.titleLevel, maxWidth, 5);
				initTextView(R.id.titleRegions, maxWidth, 5);
				initTextView(R.id.titleDiscipline, maxWidth, 5);
				initTextView(R.id.titleRanking, maxWidth, 5);
				initTextView(R.id.titleMap, maxWidth, 5);
				initTextView(R.id.titleCoordinates, maxWidth, 5);
				initTextView(R.id.titleEntriesCount, maxWidth, 5);
				
				maxWidth = rootView.findViewById(R.id.titleBank).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleDeposits).getWidth())
					maxWidth = rootView.findViewById(R.id.titleDeposits).getWidth();
				initTextView(R.id.titleDeposits, maxWidth, 5);		
				initTextView(R.id.titleBank, maxWidth, 5);				
						
				maxWidth = rootView.findViewById(R.id.titleSW).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleDirector).getWidth())
					maxWidth = rootView.findViewById(R.id.titleDirector).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleMainReferee).getWidth())
					maxWidth = rootView.findViewById(R.id.titleMainReferee).getWidth();
				if(maxWidth < rootView.findViewById(R.id.titleCourseSetter).getWidth())
					maxWidth = rootView.findViewById(R.id.titleCourseSetter).getWidth();
				initTextView(R.id.titleDirector, maxWidth, 5);		
				initTextView(R.id.titleMainReferee, maxWidth, 5);
				initTextView(R.id.titleCourseSetter, maxWidth, 5);
				initTextView(R.id.titleSW, maxWidth, 5);
		       
				ViewTreeObserver obs = titleRankingDecisionDate.getViewTreeObserver();				
		        
		        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
		            obs.removeOnGlobalLayoutListener(this);
		        } else {
		            obs.removeGlobalOnLayoutListener(this);
		        }
		        
		        
				
		    }

		});
		
		return rootView;
	}	
	
	/** 
	 * Metoda zobrazi fragment s vysledky. 
	 */
	protected void showResults() {
		EventResultsFragment eventResults = new EventResultsFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("id", eventId);
		bundle.putString("classes", ((TextView)rootView.findViewById(R.id.textViewCategoriesList)).getText().toString());
		eventResults.setArguments(bundle);
		
		FragmentManager fragmentManager = getActivity().getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.content_frame, eventResults);		
		transaction.addToBackStack(null);
		transaction.commit();
		
	}
	
	/** 
	 * Metoda zobrazi fragment s prihlaskami. 
	 */
	private void showEntries() {
		EventEntriesFragment eventEntries = new EventEntriesFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("id", eventId);
		bundle.putString("classes", ((TextView)rootView.findViewById(R.id.textViewCategoriesList)).getText().toString());
		eventEntries.setArguments(bundle);
		
		FragmentManager fragmentManager = getActivity().getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.content_frame, eventEntries);		
		transaction.addToBackStack(null);
		transaction.commit();
		
	}
	
	/** 
	 * Metoda zobrazi fragment pro prihlaseni. 
	 */
	private void login() {
		AuthFragmentEvent loginFragment = new AuthFragmentEvent();
		Bundle bundle = new Bundle();
		bundle.putInt("id", eventId);		
		loginFragment.setArguments(bundle);
		
		FragmentManager fragmentManager = getActivity().getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.content_frame, loginFragment);		
		transaction.addToBackStack(null);
		transaction.commit();
		
	}
	
	/** 
	 * Metoda zobrazi fragment pro odhlaseni. 
	 */
	private void logout() {
		LogoutFragment logoutFragment = new LogoutFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("id", eventId);		
		logoutFragment.setArguments(bundle);
		
		FragmentManager fragmentManager = getActivity().getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.content_frame, logoutFragment);		
		transaction.addToBackStack(null);
		transaction.commit();
		
	}
	

	/** 
	 * Metoda zobrazi fragment pro objednani doplnkovych sluzeb. 
	 */
	private void servicesOrder() {
		AuthFragmentServices authFragment = new AuthFragmentServices();
		Bundle bundle = new Bundle();
		bundle.putInt("id", eventId);		
		authFragment.setArguments(bundle);
		
		FragmentManager fragmentManager = getActivity().getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.content_frame, authFragment);		
		transaction.addToBackStack(null);
		transaction.commit();
		
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /** 
	 * Nastaveni zobrazeni moznosti v actionbaru
	 */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        menu.findItem(R.id.action_filter).setVisible(false);
        menu.findItem(R.id.action_order_by).setVisible(false);
        menu.findItem(R.id.action_order).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }
    
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }
    
    /** 
	 * Akce pri pouziti tlacitka na v actionbaru
	 */
    @Override
    @TargetApi(14)
    public boolean onOptionsItemSelected(MenuItem item) {    	
       // Handle action buttons
       switch(item.getItemId()) {
       case R.id.action_refresh: 
    	   //Log.i("EventFragment", "action_refresh");
    	   refresh = true;
    	   FragmentManager fragmentManager = getFragmentManager();
    	   FragmentTransaction ft = fragmentManager.beginTransaction();
    	   ft.detach(this);
    	   ft.attach(this);
    	   ft.commit();
    	   //Log.i("EventEntries", "action_refresh DONE");
    	   //Toast.makeText(this.getActivity().getApplicationContext(), "Stisknuto Obnovit", Toast.LENGTH_SHORT).show();
    	   return true;
       default:    
    	   return super.onOptionsItemSelected(item);
       }
    }	
	
    /** 
     * Metoda vyplni layout detailu zavodu daty z DB.
	 */
	private void fillLayout(){
		
		// ziskani kurzoru do databaze
		Cursor event = this.helper.getEvent(this.eventId);
		Cursor eventInfo = this.helper.getEventInfo(this.eventId);
		event.moveToFirst();
		eventInfo.moveToFirst();			
		
		/*//id zavodu
		String value = Integer.toString(id);
		addValueToLayout(R.id.valueOrisID, R.id.titleOrisId, value);*/		
		//nazev zavodu
		String value = event.getString(1);
		addValueToLayout(R.id.valueEventName, R.id.titleEventName, value);	
		getActivity().setTitle(value);
		//datum zavodu
		value = event.getString(2);
		addValueToLayout(R.id.valueDate, R.id.titleDate, value);		
		// misto konani
		value = eventInfo.getString(1);				
		addValueToLayout(R.id.valuePlace, R.id.titlePlace, value);		
		//start zavodu
		value = eventInfo.getString(2);
		addValueToLayout(R.id.valueStartTime, R.id.titleStartTime, value);		
		// Prezentace do
		value = eventInfo.getString(3);					
		addValueToLayout(R.id.valueEventOfficeCloses, R.id.titleEventOfficeCloses, value);		
		// Poradatel 
		value = "";
		Cursor clubC = this.helper.getClub(event.getInt(3));
		clubC.moveToFirst();
		if(clubC.getCount() != 0)
			value = clubC.getString(0) +" - "+ clubC.getString(1) ;
		clubC.close();
		clubC = this.helper.getClub(event.getInt(4));
		clubC.moveToFirst();
		if(clubC.getCount() != 0)
			value += "\n" + clubC.getString(0) +" - "+ clubC.getString(1) ;
		clubC.close();
		addValueToLayout(R.id.valueOrganizer, R.id.titleOrganizer,value);
		// Sport		
		value = this.helper.getStringValue("sport", "nameCZ", event.getInt(6));				
		addValueToLayout(R.id.valueSport, R.id.titleSport, value);		
		// level		
		value = this.helper.getStringValue("level", "shortName", event.getInt(8));
		value += " - " + this.helper.getStringValue("level", "nameCZ", event.getInt(8));
		addValueToLayout(R.id.valueLevel, R.id.titleLevel, value);		
		// regiony
		value = event.getString(4);	
		addValueToLayout(R.id.valueRegions, R.id.titleRegions, value);		
		// disciplina
		value =  this.helper.getStringValue("discipline", "shortName", event.getInt(7));
		value += " - " + this.helper.getStringValue("discipline", "nameCZ", event.getInt(7));
		addValueToLayout(R.id.valueDiscipline, R.id.titleDiscipline,value);		
		// Ranking
		value = event.getString(12);		
		addValueToLayout(R.id.valueRanking, R.id.titleRanking,  value);		
		// Rozhod. datum
		value = eventInfo.getString(4);
		addValueToLayout(R.id.valueRankingDecisionDate, R.id.titleRankingDecisionDate, value);		
		// Mapa
		value = eventInfo.getString(5);		
		addValueToLayout(R.id.valueMap, R.id.titleMap,value);
		// Souradnice 
		value = Double.toString(event.getDouble(14)) + ", ";
		value += Double.toString(event.getDouble(15));
		addValueToLayout(R.id.valueCoordinates, R.id.titleCoordinates, value);
		// Vklady
		value = eventInfo.getString(7);		
		addValueToLayout(R.id.valueDeposits, R.id.titleDeposits, value);
		// Banka
		value = eventInfo.getString(8);		
		addValueToLayout(R.id.valueBank, R.id.titleBank, value);
		// Sluzby
		value = eventInfo.getString(6);		
		if(value.length() != 0){
			TextView valueTextView = (TextView) rootView.findViewById(R.id.textViewServicesList);		
			valueTextView.setText(value);
		}
		else{
			rootView.findViewById(R.id.textViewServicesList).setVisibility(TextView.GONE);
		}
		// reditel zavodu
		value = eventInfo.getString(10);		
		addValueToLayout(R.id.valueDirector, R.id.titleDirector, value);
		// hlavni rozhodci
		value = eventInfo.getString(11);		
		addValueToLayout(R.id.valueReferee, R.id.titleMainReferee, value);
		// stavitel trati
		value = eventInfo.getString(12);		
		addValueToLayout(R.id.valueCourseSetter, R.id.titleCourseSetter, value);
		// zpracovatel prihlasek
		value = eventInfo.getString(13);		
		addValueToLayout(R.id.valueSW, R.id.titleSW, value);
		// Kategorie
		value = eventInfo.getString(14);		
		if(value.length() != 0){
			TextView valueTextView = (TextView) rootView.findViewById(R.id.textViewCategoriesList);		
			valueTextView.setText(value);
		}
		else{
			rootView.findViewById(R.id.textViewCategoriesList).setVisibility(TextView.GONE);
		}
		
		// pocet prihlasek a povoleni tlacitek, pokud neni pocet prihlasek 0
		if(eventInfo.getInt(15) != 0){
			rootView.findViewById(R.id.buttonEntries).setEnabled(true);	
			rootView.findViewById(R.id.buttonResults).setEnabled(true);	
		}else{
			rootView.findViewById(R.id.buttonEntries).setEnabled(false);	
			rootView.findViewById(R.id.buttonResults).setEnabled(false);	
		}
		value = Integer.toString(eventInfo.getInt(15));
		addValueToLayout(R.id.valueEntriesCount, R.id.titleEntriesCount, value);
		
		event.close();
		eventInfo.close();
		
		// ziskani kurzoru do DB pro dokumenty a odkazy
		Cursor documents = this.helper.getEventDocuments(eventId);
		Cursor links = this.helper.getEventLinks(eventId);
		documents.moveToFirst();
		links.moveToFirst();
		
		// dokumenty
		LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.linearLayoutDocuments);
		String name;
		if(documents.getCount() != 0){
			for(int i=0; i < documents.getCount(); i++){
				TextView doc = new TextView(this.getActivity());
				name = documents.getString(2);
				if(name.length() == 0)
					name = documents.getString(3);	
				
				doc.setText(Html.fromHtml("<a href=\""+documents.getString(1)+"\">"+name+"</a>"));
				doc.setMovementMethod(LinkMovementMethod.getInstance());
				
				linearLayout.addView(doc);
				documents.moveToNext();
			}
		}
			
		// odkazy
		linearLayout = (LinearLayout) rootView.findViewById(R.id.linearLayoutLinks);
		if(links.getCount() != 0){
			for(int i=0; i < links.getCount(); i++){
				TextView link = new TextView(this.getActivity());
				name = links.getString(2);
				if(name.length() == 0)
					name = links.getString(3);							
				link.setText(Html.fromHtml("<a href=\""+links.getString(1)+"\">"+links.getString(2)+"</a>"));
				link.setMovementMethod(LinkMovementMethod.getInstance());
				linearLayout.addView(link);
				links.moveToNext();
			}
		}
		
		documents.close();
		links.close();
		
	}	
	
	/** 
	 * Metoda z OrisAPIListener, ktera se asynchrone vola z OrisAPI se stazenymi daty.
	 * Nacte data do databaze a pote vola fillLayout()
     * @param data Data z weboveho API jako JSONObject
     * @param method Typ dotazu (OrisAPI.Method)
     */
	@Override
	public void onDownloadComplete(JSONObject data, Method method) {
				
		switch(method){
			case GET_EVENT:				
				try {
					// nacteni detailu zavodu do databaze
					JSONObject dataObj = data.getJSONObject("Data");					
					String url="", nameCZ="", otherDescCZ="";
					int id;
					if(dataObj.has("Documents") && (dataObj.optJSONObject("Documents") != null)){
						JSONObject docsObj = dataObj.getJSONObject("Documents");
						Iterator<String> keys= docsObj.keys();
						while(keys.hasNext()){
							String key = keys.next();
							id = docsObj.getJSONObject(key).getInt("ID");
							url = docsObj.getJSONObject(key).getString("Url");	
							if((docsObj.getJSONObject(key).optJSONObject("SourceType") != null))
								nameCZ = docsObj.getJSONObject(key).getJSONObject("SourceType").getString("NameCZ");
							else
								otherDescCZ = docsObj.getJSONObject(key).getString("OtherDescCZ");
							if (!(this.helper.recordExists("documents", id))) {
								this.helper.insertDocuments(id, url, nameCZ, otherDescCZ, eventId);
							}
							else{
								this.helper.updateDocuments(id, url, nameCZ, otherDescCZ, eventId);
							}
						}	
						//Log.i("EventFragment", "documents ok");
					}	
					
					if(dataObj.has("Links") && (dataObj.optJSONObject("Links") != null)){
						JSONObject linksObj = dataObj.getJSONObject("Links");
						Iterator<String> keys= linksObj.keys();
						while(keys.hasNext()){
							String key = keys.next();
							id = linksObj.getJSONObject(key).getInt("ID");
							url = linksObj.getJSONObject(key).getString("Url");	
							if((linksObj.getJSONObject(key).optJSONObject("SourceType") != null))
								nameCZ = linksObj.getJSONObject(key).getJSONObject("SourceType").getString("NameCZ");
							else
								otherDescCZ = linksObj.getJSONObject(key).getString("OtherDescCZ");
							if (!(this.helper.recordExists("links", id))) {
								this.helper.insertLinks(id, url, nameCZ, otherDescCZ, eventId);
							}
							else{
								this.helper.updateLinks(id, url, nameCZ, otherDescCZ, eventId);
							}
						}	
					}
					
					String place="", startTime="", eventOfficeCloses="", rankingDecisionDate="", map="", services="", 
							entryDescCZ="", entryBankAccount="", entryInfo="", director="", mainReferee="", courseSetter="", 
							sw="", categories="";
					int entriesCount = 0;
					if(dataObj.has("Place"))
						place = dataObj.getString("Place");
					if(dataObj.has("StartTime"))
						startTime = dataObj.getString("StartTime");
					if(dataObj.has("EventOfficeCloses"))
						eventOfficeCloses = dataObj.getString("EventOfficeCloses");
					if(dataObj.has("RankingDecisionDate"))
						rankingDecisionDate = dataObj.getString("RankingDecisionDate");
					if(dataObj.has("Map"))
						map = dataObj.getString("Map");
					if(dataObj.has("Services") && (dataObj.optJSONObject("Services") != null)){
						JSONObject servicesObj = dataObj.getJSONObject("Services");
						Iterator<String> keys= servicesObj.keys();
						while(keys.hasNext()){
							String key = keys.next();
							services += servicesObj.getJSONObject(key).getString("NameCZ") + " (";
							services += servicesObj.getJSONObject(key).getString("UnitPrice") + ")";
							if(keys.hasNext())
								services += ", ";
						}						
					}
					if(dataObj.has("EntryDescCZ"))
						entryDescCZ = dataObj.getString("EntryDescCZ");
					if(dataObj.has("EntryBankAccount"))
						entryBankAccount = dataObj.getString("EntryBankAccount");					
					if(dataObj.has("EntryInfo"))
						entryInfo = dataObj.getString("EntryInfo");
					if(dataObj.has("Director") && (dataObj.optJSONObject("Director") != null)){
						director = dataObj.getJSONObject("Director").getString("FirstName")+ " ";						
						director += dataObj.getJSONObject("Director").getString("LastName");
					}
					if(dataObj.has("MainReferee") && (dataObj.optJSONObject("MainReferee") != null)){
						mainReferee = dataObj.getJSONObject("MainReferee").getString("FirstName")+ " ";						
						mainReferee += dataObj.getJSONObject("MainReferee").getString("LastName");
					}
					if(dataObj.has("CourseSetter_1") && (dataObj.optJSONObject("CourseSetter_1") != null)){
						courseSetter = dataObj.getJSONObject("CourseSetter_1").getString("FirstName")+ " ";						
						courseSetter += dataObj.getJSONObject("CourseSetter_1").getString("LastName");
					}	
					if(dataObj.has("SW") && (dataObj.optJSONObject("SW") != null)){
						sw = dataObj.getJSONObject("SW").getString("FirstName")+ " ";						
						sw += dataObj.getJSONObject("SW").getString("LastName");
					}	
					if(dataObj.has("Classes") && (dataObj.optJSONObject("Classes") != null)){
						JSONObject classesObj = dataObj.getJSONObject("Classes");
						Iterator<String> keys= classesObj.keys();
						String name;
						double distance;
						int classId, climbing, controls;	
						List<String> categoriesList = new ArrayList<String>();
						while(keys.hasNext()){
							String key = keys.next();
							name = classesObj.getJSONObject(key).getString("Name");
							classId = classesObj.getJSONObject(key).getInt("ID");
							distance = classesObj.getJSONObject(key).getDouble("Distance");
							climbing = classesObj.getJSONObject(key).getInt("Climbing");
							controls = classesObj.getJSONObject(key).getInt("Controls");
							entriesCount += classesObj.getJSONObject(key).getInt("CurrentEntriesCount");
							
							if(this.helper.getClass(name).getCount() == 0){
								if (!(this.helper.recordExists("classes", classId))) {
									this.helper.insertClass(classId, name, distance, climbing, controls);
								}
								else{
									this.helper.updateClass(classId, name, distance, climbing, controls);
								}
									
							}
							
													
							categoriesList.add(name);							
						}
						
						Collections.sort(categoriesList);
						keys = categoriesList.iterator();
						while(keys.hasNext()){
							categories += keys.next();
							if(keys.hasNext())
								categories += ", ";
						}						
					}
					
					//Log.d("EventFragment","bank"+entryBankAccount);					
					
					if (!(this.helper.recordExists("eventInfo", eventId))) {
						this.helper.insertEventInfo(eventId, place, startTime, eventOfficeCloses, 
								rankingDecisionDate, map, services, entryDescCZ, entryBankAccount, entryInfo, director, 
								mainReferee, courseSetter, sw, categories, entriesCount);
					}else{
						//Log.d("EventFragment","update");	
						this.helper.updateEventInfo(eventId, place, startTime, eventOfficeCloses, 
								rankingDecisionDate, map, services, entryDescCZ, entryBankAccount, entryInfo, director, 
								mainReferee, courseSetter, sw, categories, entriesCount);
					}
					
					// nahrani dat do layoutu
					fillLayout();
					
				} catch (JSONException e) {					
					e.printStackTrace();
				}		
				break;				
			case UNABLE_RETRIEVE_DATA:
				
			break;
				
			default:
				break;
		}		
	}
	
	/** 
	 * Metoda odsadi text od popisku.
	 * @param id Id popisku
	 * @param maxWidth nejdelsi delka popisku
	 * @param plusMargin dodatecne odsazeni 
     */
	private void initTextView(int id, int maxWidth, int plusMargin){
		TextView textView = (TextView) rootView.findViewById(id);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    params.setMargins(0, 0, maxWidth+plusMargin-textView.getWidth(), 0);
	    textView.setLayoutParams(params);		
	}
	
	/** 
	 * Vlozeni informaci do layout.
	 * Pokud nejaka informace chybi nezobrazi popisek
	 * @param id Id textView pro vlozeni
	 * @param idTitle Id popisku
	 * @param value Text pro vlozeni 
     */
	private void addValueToLayout(int idValue, int idTitle,String value){
		if(value.length() != 0){
			TextView valueTextView = (TextView) rootView.findViewById(idValue);		
			valueTextView.setText(value);
		}
		else{
			rootView.findViewById(idValue).setVisibility(TextView.GONE);
			rootView.findViewById(idTitle).setVisibility(TextView.GONE);
		}
	}
	
//	@Override
//	public void onDestroyView() {
//		Log.i("CalendarListFragment", "onDestroyView");
//		
//		super.onDestroyView();		
//		helper.close();
//	}

}

