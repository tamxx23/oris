package cz.vutbr.fit.tam.oris;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Event {
	
	public int id;
	public String name;
	public Date date;
	public String date_str;
	public String[] organizers;
	public String organizer;
	public String region;
	public String[] regions;
	public int sport_id;
	public String sport_str;
	public int discipline_id;
	public String discipline_str;
	public int level_id;
	public String level_str;
	public String[] entry_date;
	public int ranking;
	public int cancelled;
	public double latitude;
	public double longitude;
	
	/** Konstruktor 
	 * @throws ParseException */
	public Event(int id, String name, String date, String org) throws ParseException {
		this.id = id;
		this.name = name;
		this.date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(date);
		// date_str obsahuje datum ve formatu dd.MM.
		this.date_str = new SimpleDateFormat("dd.MM.", Locale.getDefault()).format(this.date);
		this.organizer = org;		
	}

}

/** Ukázka výstupu z ORIS API při GET_EVENT_LIST - pouze 1 závod, kterých může být ve výstupu více
"Event_2405": {
    "ID": "2405",
    "Name": "\u010cesk\u00fd poh\u00e1r, \u017deb\u0159\u00ed\u010dek A a B",
    "Date": "2014-02-15",
    "Org1": {
        "ID": "106",
        "Abbr": "OSN",
        "Name": "SK Orienta\u010dn\u00ed sporty Nov\u00e9 M\u011bsto na Morav\u011b"
    },
    "Org2": {
        "ID": "169",
        "Abbr": "TBM",
        "Name": "KOS TJ Tesla Brno"
    },
    "Region": "\u010cR",
    "Regions": {
        "Region_\u010cR": {
            "ID": "\u010cR",
            "Name": "\u010cR"
        }
    },
    "Sport": {
        "ID": "2",
        "NameCZ": "LOB",
        "NameEN": "Ski O"
    },
    "Discipline": {
        "ID": "2",
        "ShortName": "KT",
        "NameCZ": "Kr\u00e1tk\u00e1 tra\u0165",
        "NameEN": "Middle distance"
    },
    "Level": {
        "ID": "8",
        "ShortName": "\u010cP",
        "NameCZ": "\u010cesk\u00fd poh\u00e1r",
        "NameEN": "Czech cup"
    },
    "EntryDate1": "2014-02-03 23:59:59",
    "EntryDate2": "",
    "EntryDate3": "",
    "Ranking": "0",
    "Cancelled": "1",
    "GPSLat": "49.6103",
    "GPSLon": "16.0084"
}
*/