package cz.vutbr.fit.tam.oris;

import java.util.ArrayList;
import java.util.List;


/** Trida pro ulozeni tabulky prihlasek/vysledku pro 
 * danou kategorii.
 */
public class ClassTable {
	
	/** Kategorie */
	public Category category;
	/** Tabulka prihlasek/vysledku. Prvni plozka je vzdy hlavicka.*/
	public List<List<String>> table;
	
	/** 
	 * Konstruktor.
	 */
	public ClassTable(){
		table = new ArrayList<List<String>>();
	}
	
	/** Trida pro kategorii.
	 */
	public class Category {
		/** Id kategorie */
		public int classId;
		/** Nazev kategorie */
		public String className;
		
		/**Informace o kategorii*/
		public double distance;
		/**Informace o kategorii*/
		public int climbing;
		/**Informace o kategorii*/
		public int controls;
				

		/** 
		 * Metoda vytvoreni popis pro kategorii.
		 * @return Popis kategorie
		 */
		public String getDescription(){
			String description = "";
			if(distance != 0)
				description += distance + " km";
			if(climbing != 0)
				description += ", "+climbing+" m";
			if(controls != 0)
				description += ", "+controls+" kontrol";
			return description;
		}
		
	}
}
