package cz.vutbr.fit.tam.oris;

import java.util.List;

import org.json.JSONObject;

import android.widget.Toast;
import cz.vutbr.fit.tam.oris.OrisAPI.Method;

public class User{
	private static User instance = null;	
	private String firstName;
	private String lastName;
	private int userId;	
	private String userName;
	private String password;
	private String license;
		
	protected User() {			
	}
	
	public User(String mUserName, String mPassword, String mFirstName, String mLastName, int mUserId, String mLicense) {	
		
		instance = new User();
		
		instance.firstName = mFirstName;
		instance.lastName = mLastName;
		instance.userId = mUserId;
		instance.userName = mUserName;
		instance.password = mPassword;
		instance.license = mLicense;
	}
	
	public static User getUser() {	      
	      return instance;
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public int getuserId(){
		return userId;
	}
	
	public String getUserName(){
		return userName;
	}
	
	public String getPassword(){
		return password;
	}
	
	public String getLicense(){
		return license;
	}
}
