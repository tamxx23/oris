package cz.vutbr.fit.tam.oris;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONObject;

import cz.vutbr.fit.tam.oris.OrisAPI.Method;
import android.app.Fragment;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

/** Abstraktní třída pro fragmenty s prihlaskami a vysledky pro dany zavod.
 * Jde o fragment obsahujici seznam kategorii (ExpandableList) a 
 * kazdy radek obdahuje tabulku (TableLayout) pro danou kategorii.
 * 
 * Odvozene tridy implementuji abstraktni metody:
 *     onDownloadComplete(JSONObject data, Method method) - kazda trida potrebuje stahnout jina data
 *     Cursor getDBList(int classId) - kazda trida ma dotaz na jinou tabulku/jina data v DB, ze ktere 
 *     		se cte tabulka
 *     List<String> addHead() - ruzna hlavicka tabulky (hlavicka vysledku vs hlavicka prihlasek)
 *     List<String> addRow(Cursor list) - kazdy fragment potrebuje ma jina data, ktera zapisuje do tabulky 
 * 
 * O stazeni dat ze serveru se stara odvozena trida. V teto tride BaseListFragment se provadi
 * vytvoreni seznamu mExpList, ktery obsahuje tabulku kazde kategorie a popis kategorie.
 * Tabulka se vytvori a vlozi do seznamu mExpList v metode getDataFromDatabase().
 * Pri plneni seznamu mExpList (ClassTable obsahuje List<List<String>> coz je tabulka vysledku/prihlasek)
 * daty se zavola getDBList(int classId) odvozene tridy, ktera vrati kurzor na vysledek dotazu na DB (pocet 
 * sloupcu ve vykreslene tabulce odpovida poctu sloupcu tohoto dotazu). Odvozena trida pak doplni do ClassTable
 * vlastni hlavicku (addHead(), 0-ty prvek seznamu List<List<String>> je vzdy hlavicka) a 
 * radky s daty (addRow(Cursor list)), ktere cte ze ziskaneho kurzoru. 
 * Takto vytvoreny seznam mExpList preda TablesExpListAdapter, ktery vykresli seznam s tabulkami. 
 */
public abstract class BaseListFragment extends Fragment implements OrisAPIListener{

	/** Pristup k serveru */
	protected OrisAPI api;
	/** Pristup k databazi */
	protected OrisHelper helper;
	/** Layout fragment */
	protected View mainView;
	/** id zavodu */
	protected int eventId;
	/** Seznam kategorii v zavodu */
	protected List<String> classes;
	/** Seznam kategorii s popisem a tabulkou */
	protected List<ClassTable> mExpList;	
	
	
	/** Metoda onCreateView se vola pri vytvoreni fragmentu.
	 * Inicializuje pristup k serveru, DB. Prijme seznam trid z detailu zavodu
	 * a inicializuje seznam classes.
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return view fragmentu
	 */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) { 
    	Bundle bundle = this.getArguments();
    	this.eventId = bundle.getInt("id");
		this.helper = new OrisHelper(this.getActivity());
		this.api = new OrisAPI(this.getActivity(), this);
		mExpList = new ArrayList<ClassTable>();
		
		classes = new ArrayList<String>();
		String[] classStr  = bundle.getString("classes").replace(" ", "").split(",");		
		for(int i = 0; i<classStr.length; i++){
			classes.add(classStr[i]);
		}
		Collections.sort(classes);
				
    	return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    /** Zobrazi seznam kategorii s tabulkami, nastavi titulek v actionbaru. 
	 */
    protected void displayList() {
		setTitle();
		ExpandableListView mExpListView;
		TablesExpListAdapter mTablesExpListAdapter;
		try {
			if (mExpList != null) {	        	
	        	// Nastaveni ExpandableListView 
	            mExpListView = (ExpandableListView) mainView.findViewById(R.id.list_results);
	            mTablesExpListAdapter = new TablesExpListAdapter(this.getActivity(), mExpList);
	            mExpListView.setAdapter(mTablesExpListAdapter);	            
	        } else {
	        	Toast.makeText(this.getActivity().getApplicationContext(), "Žádné nalezené přihlášky", Toast.LENGTH_LONG).show();
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
    
    /** Inicializuje seznam mExpList.
	 * Prochazi seznam kategorii pro dany zavod (classes) a pro kazdou kategorii
	 * zjisti jeji popis. Odvozena trida ziska pro dany zavod a kategorii data z 
	 * DB (getDBList(int classId)) a vytvori tabulku (List<List<String>> v ClassTable) 
	 * metodami addHead() a addRow(Cursor list).
	 */    
    protected void getDataFromDatabase() {
		for(int i=0; i < classes.size(); i++){
			// vytvoreni nove kategorie
			ClassTable classTable = new ClassTable();	
			classTable.category = classTable.new Category();
			
			Cursor classEvent = this.helper.getClass(classes.get(i));	
			classEvent.moveToFirst();
			if(classEvent.getCount() != 0){
				// informace o kategorii
				classTable.category.classId = classEvent.getInt(0);
				classTable.category.className = classEvent.getString(1);
				classTable.category.distance = classEvent.getDouble(2);
				classTable.category.climbing = classEvent.getInt(3);
				classTable.category.controls = classEvent.getInt(4);			
				
				// data do tabulky
				Cursor list = getDBList(classTable.category.classId);			
				list.moveToFirst();
				
				// hlavicka tabulky
				classTable.table.add(addHead());						

				if(list.getCount() != 0){
					// vytvoreni tabulky
					for(int n=0; n < list.getCount(); n++){	
						classTable.table.add(addRow(list));
						list.moveToNext();
					}
					mExpList.add(classTable);
					list.close();
				}
				classEvent.close();
			}			
		}		
	}
    
    /** Abstraktni metoda pro ziskani dat z DB.
     * Vraci kurzor na data, ktera se maji vlozit do tabulky.
	 * @param classId Id kategorie
	 * @return Kurzor na vyseldek dotazu do DB
	 */    
    abstract protected Cursor getDBList(int classId); 
    
    /** Abstraktni metoda pro nastaveni titulku v actionbaru.
	 */ 
    abstract protected void setTitle();
    
    /** Abstraktni metoda pro vytvoreni hlavicky tabulky.
     *  @return Seznam stringu (hlavicka tabulky)
	 */ 
    abstract protected List<String> addHead();
    
    /** Abstraktni metoda pro vytvoreni radku tabulky.
     * @return Seznam stringu (radek tabulky)
	 */ 
    abstract protected List<String> addRow(Cursor list);
    
    /** Abstraktni metoda listener na stazena data ze serveru.
     * @param data Data ve Json formatu.
     * @param method Typ dat, ktera se vraci.
	 */
    @Override
    abstract public void onDownloadComplete(JSONObject data, Method method);
    
//    @Override
//	public void onDestroyView() {		
//		
//		super.onDestroyView();
//		// ukonceni ukazatele na databazi
//		helper.close();
//	}
}
