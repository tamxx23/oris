package cz.vutbr.fit.tam.oris;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.List;

/**
 * Pro komunikaci s API je potreba svoji tridu implementovat jako intreface OrisAPIListener 
 * s metodou onDownloadComplete(), ktera se asynchrone vola z OrisAPI, pri prijeti vysledku
 * z weboveho API.
 */
public class MainActivity extends ActionBarActivity {
	
	public static boolean DEBUG = false;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle; // nazev aplikace
    private CharSequence mTitle; // nazev aktualniho zobrazeni
    
    private String[] mNavigationOptionsTitles;
    
    private CalendarListFragment mListFragment;
    private BaseAuthFragment mLoginFragment;
    private OrisMapFragment mMapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        mTitle = mDrawerTitle = getTitle();
        mNavigationOptionsTitles = getResources().getStringArray(R.array.navigation_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        
        /** Nastaveni navigacni sipky na Action Baru */        
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        
        /** Nastaveni zastineni pri zobrazeni Navigation drawer */
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        /** Nastaveni listu pro navigation drawer */
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item_small, mNavigationOptionsTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        /** Nastaveni moznosti "otevirani" a "zavirani" navigation draweru */
        /**
         * @param this Hostitelska aktivita
         * @param mDrawerLayout 
         * @param R.drawable.ic_drawer Obrazek misto sipky
         * @param Popis "otevreni" navigation draweru
         * @param Popis zavreni
         */
        mDrawerToggle = new ActionBarDrawerToggle( this, mDrawerLayout, R.string.drawer_open,  R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };
        
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        
        mListFragment = new CalendarListFragment();
        mLoginFragment = new AuthFragmentEvent();
        mMapFragment = new OrisMapFragment();
        
        if (savedInstanceState == null) {
            selectItem(0);
        }       
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_refresh).setVisible(!drawerOpen);
        menu.findItem(R.id.action_filter).setVisible(!drawerOpen);
        menu.findItem(R.id.action_order_by).setVisible(!drawerOpen);
        menu.findItem(R.id.action_order).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Informační dialog
     * @param title Název
     * @param message Zpráva v dialogu
     */
    private void showAbout(int title, int message) {
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage(message).setTitle(title);

    	Dialog d = builder.create();
    	d.setCanceledOnTouchOutside(true);
    	d.show();
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) { 	
       if (mDrawerToggle.onOptionsItemSelected(item)) {
           return true;
       }

       // Tlačítka v action baru
       switch(item.getItemId()) {
       case R.id.about_app:
    	   showAbout(R.string.welcome, R.string.app_msg);
           return true;
       case R.id.about_authors:
    	   showAbout(R.string.about_authors, R.string.authors_msg);
           return true;
       default:
           return super.onOptionsItemSelected(item);
       }
    }
    
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    /**
     * Výběr položky v Navigation Drawer
     * @param position
     */
    private void selectItem(int position) {
    	FragmentManager fragmentManager = getFragmentManager();
    	FragmentTransaction ft = fragmentManager.beginTransaction();
    	switch(position) {
    	// Seznam
    	case 0:
            ft.replace(R.id.content_frame, mListFragment, "CalendarList").commit();

            // zmenit na vybranou polozku, zmenit titul a zavrit drawer
            mDrawerList.setItemChecked(position, true);
            setTitle(mNavigationOptionsTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);

    		break;
    	// Mapa
    	case 1:
    		// ziskani souradnic pro mapu
    		List<Event> calendarEvents = mListFragment.getCalendarEvents();
    		//bundle pro predani informací pro mapu
    		Bundle bundle = new Bundle();
    		if (!calendarEvents.isEmpty()) {
	    		
	    		double[] longArray = new double[calendarEvents.size()];
	    		double[] latArray = new double[calendarEvents.size()];
	    		String[] nameArray = new String[calendarEvents.size()];
	    		String[] clubArray = new String[calendarEvents.size()];

	    		// plneni bundlu
	    		for(int i=0; i<calendarEvents.size();i++){
	    			longArray[i] = calendarEvents.get(i).longitude;
	    			latArray[i] = calendarEvents.get(i).latitude;
	    			nameArray[i] = calendarEvents.get(i).name;
	    			clubArray[i] = calendarEvents.get(i).organizer;
	    		}
	    		bundle.putDoubleArray("lon", longArray);
	    		bundle.putDoubleArray("lat", latArray);
	    		bundle.putStringArray("name", nameArray);
	    		bundle.putStringArray("club", clubArray);
    		}
    		
    		if (!mMapFragment.isAdded()) {
    			mMapFragment.setArguments(bundle);	
    		}
    		
            ft.replace(R.id.content_frame, mMapFragment, "Map").commit();
    		
    		// zmenit na vybranou polozku, zmenit titul a zavrit drawer
            mDrawerList.setItemChecked(position, true);
            setTitle(mNavigationOptionsTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
    		break;
    	// Prihlasovani
    	/*case 2:
            ft.replace(R.id.content_frame, mLoginFragment, "Login").commit();
    		
    		// zmenit na vybranou polozku, zmenit titul a zavrit drawer
            mDrawerList.setItemChecked(position, true);
            setTitle(mNavigationOptionsTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
    		break;*/
    	default:
    		break;
    	}
        
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(title);        
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    @Override
    public void onBackPressed()
    {    	
    	if (getFragmentManager().getBackStackEntryCount() != 0) {
    		getFragmentManager().popBackStack();
    		return;
    	}
        super.onBackPressed();
    }
	
	@Override
	public void onDestroy(){
		SharedPreferences mSharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = mSharedPref.edit();
		editor.remove(getString(R.string.month));
		editor.remove(getString(R.string.year));
		editor.remove(getString(R.string.action_order));
		editor.commit();
		super.onDestroy();
	}

}
