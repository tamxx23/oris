package cz.vutbr.fit.tam.oris;

import org.json.JSONObject;

import cz.vutbr.fit.tam.oris.OrisAPI.Method;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ServicesFragment extends Fragment implements OrisAPIListener{

	private View rootView;
	private int eventId;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
				
		rootView = inflater.inflate(R.layout.fragment_services, container, false);
		Bundle bundle = this.getArguments();		
		eventId = bundle.getInt("id");
	
		return rootView;
	}
	
	@Override
	public void onDownloadComplete(JSONObject data, Method method) {
		
		
	}

}
