package cz.vutbr.fit.tam.oris;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

public class AuthFragmentEvent extends BaseAuthFragment{

	@Override
	protected void nextFragment() {		
		EventLoginFragment eventLogin = new EventLoginFragment();					
		Bundle bundle = new Bundle();
		bundle.putInt("id", eventId);						
		eventLogin.setArguments(bundle);		
		
		FragmentManager fragmentManager = getActivity().getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.content_frame, eventLogin);		
		transaction.addToBackStack(null);
		transaction.commit();
	}

}
