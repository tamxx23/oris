package cz.vutbr.fit.tam.oris;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.vutbr.fit.tam.oris.OrisAPI.Method;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

/** 
 * Fragment pro zobrazení seznamu závodů.
 * Načte informace z databáze (tabulka events) a naplní layout daty. 
 * V případě, že data nejsou v databázi, tak se nejprve stáhnout ze serveru ORIS API.
 */
public class CalendarListFragment extends Fragment implements OrisAPIListener {

	private OrisAPI api;
	private OrisHelper helper;
	private List<Event> mCalendarEvents;
    private ExpandableListView mExpListView;
    private ExpandableListAdapter mExpListAdapter;
    private View mainView;
    
    private SharedPreferences mSharedPref;
    private String dateFromStr;
    private String dateToStr;
    
    private int monthNow;

    private boolean refresh = false;
    private boolean eventsNull = false;
    
	/** Konstruktor */
	public CalendarListFragment() {

	}
	
	/**
	 * Vrátí seznam závodů
	 * @return Seznam závodů
	 */
	public List<Event> getCalendarEvents(){
		return mCalendarEvents;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (MainActivity.DEBUG) {
			Log.d("CalendarListFragment", "onCreateView");
		}
		
		mainView = inflater.inflate(R.layout.fragment_calendar_list, container, false);
		
		/** Inicializace potřebných proměnných */
		this.helper = new OrisHelper(this.getActivity());
		this.api = new OrisAPI(this.getActivity(), this);
		mCalendarEvents = new ArrayList<Event>();
		mSharedPref = getActivity().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		
		/** Vložení sportů, disciplín a úrovní do databáze */
        if (helper.tableCount("sport") == 0) {
        	Log.i("set permanent data","data");
        	insertInitDataToDatabase();
        }

        if (MainActivity.DEBUG) {
	        Log.i("tableCount sport", Integer.toString(this.helper.tableCount("sport")));
	        Log.i("tableCount discipline", Integer.toString(this.helper.tableCount("discipline")));
	        Log.i("tableCount level", Integer.toString(this.helper.tableCount("level")));
        }
        
        /** Nastavení datumů pro zobrazení seznamu */
        initDates();
        
        getActivity().setTitle("Seznam");

        /** Vložení všech klubů do databáze */
        if (this.helper.tableCount("club") == 0) {
	        try {
	        	api.getCSOSClubList();
			} catch (Exception e) {			
				Toast.makeText(this.getActivity().getApplicationContext(), "Nemáš připojení k INTERNETu", Toast.LENGTH_SHORT).show();
				Log.d("MainActivity: ", e.getMessage());
			} 
        }

        if (helper.getEventsCount(dateFromStr, dateToStr) == 0 || refresh) {
        	/** Načtení dat z API do databáze a jejich zobrazení */
        	downloadEvents();
        	refresh = false;
        } else {
        	/** Stažení vyfiltrovaných dat (podle výběru) z databáze */
        	getEventsFromDatabase();
        	/** Seřadit seznam vzestupně/sestupně */
        	orderList();
        	/** Zobrazit seznam závodů */
        	displayList();	
        }
        return mainView;

	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) { 	
        super.onCreateOptionsMenu(menu, inflater);
    }
    
    @Override
    @SuppressLint("NewApi")
    public boolean onOptionsItemSelected(MenuItem item) {    	
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
      
       // Handle action buttons
       switch(item.getItemId()) {
       case R.id.action_refresh:
    	   refresh = true;
    	   FragmentManager fragmentManager = getFragmentManager();
    	   FragmentTransaction ft = fragmentManager.beginTransaction();
    	   ft.detach(this);
    	   ft.attach(this);
    	   ft.commit();
    	   return true;
       case R.id.action_filter:
    	   filterDialog();
           return true;
       case R.id.action_order_by:
    	   orderByDialog();
           return true;
       case R.id.action_order:
    	   /** Seradit sestupne/vzestupne */ 	   
    	   Collections.reverse(mCalendarEvents);
    	   if (item.getTitle() == "Seřadit vzestupně") {
    		   item.setIcon(this.getResources().getDrawable(R.drawable.ic_action_hardware_keyboard_arrow_down));
    		   item.setTitle("Seřadit sestupně");
    		   SharedPreferences.Editor editor = mSharedPref.edit();
    		   editor.putString(getString(R.string.action_order), "vzestupně");
    		   editor.commit();
    	   } else {
    		   item.setIcon(this.getResources().getDrawable(R.drawable.ic_action_hardware_keyboard_arrow_up));
    		   item.setTitle("Seřadit vzestupně");
    		   SharedPreferences.Editor editor = mSharedPref.edit();
    		   editor.putString(getString(R.string.action_order), "sestupně");
    		   editor.commit();
    	   }
    	   displayList();
           return true;
       default:
    	   return super.onOptionsItemSelected(item);
       }
       
    }
	
	/** 
	 * Zobrazí seznam závodů
	 */
	private void displayList() {
		try {
			if (mCalendarEvents != null) {
	        	// Nastaveni ExpandableListView 
	            mExpListView = (ExpandableListView) mainView.findViewById(R.id.list_calendar);
	            mExpListAdapter = new ExpandableListAdapter(this.getActivity(), mCalendarEvents);
	            mExpListView.setAdapter(mExpListAdapter);           
	        } else {
	        	Log.d("CalendarListFragment", "displayList mCalendarEvents == null");
	        	Toast.makeText(this.getActivity().getApplicationContext(), "Žádné nalezené závody", Toast.LENGTH_SHORT).show();
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Inicializuje privátní proměnné dateFromStr a dateToStr
	 * na rozmezí +-7 dní od dnes
	 */
	private void initDates() {
    	Calendar cal = Calendar.getInstance();
    	Date dateNow = cal.getTime();
    	this.monthNow = Integer.parseInt(new SimpleDateFormat("MM", Locale.getDefault()).format(dateNow));
    	cal.add(Calendar.DATE, -7);
    	Date dateFrom = cal.getTime();
    	cal.add(Calendar.DATE, 14);
    	Date dateTo = cal.getTime();
    	
    	if (MainActivity.DEBUG) {
	    	Log.d("dateFrom", dateFrom.toString());
	    	Log.d("dateTo", dateTo.toString());
    	}

    	if (this.dateFromStr == null) {
    		this.dateFromStr = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(dateFrom);
    	}
    	if (this.dateToStr == null) {
    		this.dateToStr = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(dateTo);
    	}
	}
	
	/**
	 * Nastaví vybraný rok z filtrace do datumu pro zobrazení seznamu.
	 * @param year Vybraný rok
	 */
	private void setYear(String year) {
		this.dateFromStr = year + dateFromStr.substring(4);
		this.dateToStr = year + dateToStr.substring(4);

		if (MainActivity.DEBUG) {
			Log.i("dateFromStr", dateFromStr);
		}
	}	

    /**
     * Zobrazí dialog pro filtraci závodů podle roku, měsíce, typu a regionu.
     */
    private void filterDialog() {
    	LayoutInflater inflater = (LayoutInflater) this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	View layout = inflater.inflate(R.layout.filter_layout, (ViewGroup) getView().findViewById(R.id.filter_layout));
		final Spinner months = (Spinner) layout.findViewById(R.id.month_spinner);
		Spinner regions = (Spinner) layout.findViewById(R.id.region_spinner);
		
		// Ukládání zvoleného nastavení
		final SharedPreferences.Editor editor = mSharedPref.edit();
		
		RadioGroup year = (RadioGroup) layout.findViewById(R.id.rg_year);
		year.clearCheck();		
		if (dateFromStr.contains("2013")) {
			year.check(mSharedPref.getInt(getString(R.string.year), R.id.radio_2013));
		} else if (dateFromStr.contains("2014")) {
			year.check(mSharedPref.getInt(getString(R.string.year), R.id.radio_2014));
		} else if (dateFromStr.contains("2015")) {
			year.check(mSharedPref.getInt(getString(R.string.year), R.id.radio_2015));
		} 
			
		year.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {	
				eventsNull = false;
				Log.d("CalendarListFragment", "onCheckedChanged ROK");
				switch(checkedId) {
				case R.id.radio_2013:
					setYear("2013");
					break;
				case R.id.radio_2014:
					setYear("2014");
					break;
				case R.id.radio_2015:
					setYear("2015");
					break;
				default:
					break;
				}
				editor.commit();
				getEventsFromDatabase();
				orderList();
            	displayList();
			}
		});
		
		RadioGroup sport = (RadioGroup) layout.findViewById(R.id.rg_sport);	
		sport.clearCheck();
		sport.check(mSharedPref.getInt(getString(R.string.sport_id), R.id.all));
		sport.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				eventsNull = false;
				Log.d("CalendarListFragment", "onCheckedChanged SPORT");
				SharedPreferences.Editor editor = mSharedPref.edit();				
				
				switch(checkedId) {
				case R.id.all:					
					editor.putInt(getString(R.string.sport), 0);
					editor.putInt(getString(R.string.sport_id), R.id.all);
					break;
				case R.id.ob:					
					editor.putInt(getString(R.string.sport), 1);
					editor.putInt(getString(R.string.sport_id), R.id.ob);
					break;
				case R.id.lob:
					editor.putInt(getString(R.string.sport), 2);
					editor.putInt(getString(R.string.sport_id), R.id.lob);
					break;
				case R.id.mtbo:
					editor.putInt(getString(R.string.sport), 3);
					editor.putInt(getString(R.string.sport_id), R.id.mtbo);
					break;
				case R.id.trail:
					editor.putInt(getString(R.string.sport), 4);
					editor.putInt(getString(R.string.sport_id), R.id.trail);
					break;
				default:
					break;
				}
				
				editor.commit();
				getEventsFromDatabase();
				orderList();
            	displayList();
			}
		});

		ArrayAdapter<CharSequence> monthsAdapter = ArrayAdapter.createFromResource(getActivity(),
		        R.array.months_array, android.R.layout.simple_spinner_item);
		monthsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		months.setAdapter(monthsAdapter);
		months.setSelection(mSharedPref.getInt(getString(R.string.month), monthNow-1));
		
		months.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				eventsNull = false;
				Log.d("CalendarListFragment", "onItemSelected MĚSÍC");
				
				SharedPreferences.Editor editor = mSharedPref.edit();
				editor.putInt(getString(R.string.month), position);
				editor.commit();

				String month;
				if (position < 10) {
					month = "0" + String.valueOf(position+1);
				} else {
					month = String.valueOf(position+1);
				}

				dateFromStr = dateFromStr.substring(0,5) + month + "-01";
				dateToStr = dateToStr.substring(0,5) + month + "-31";

				if (MainActivity.DEBUG) {
					Log.d("dateFromStr", dateFromStr);
				}
				getEventsFromDatabase();
				orderList();
				displayList();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
	
			}
			
		});
		
		ArrayAdapter<CharSequence> regionsAdapter = ArrayAdapter.createFromResource(this.getActivity(),
				R.array.regions_array, android.R.layout.simple_spinner_item);
		regionsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		regions.setAdapter(regionsAdapter);
		regions.setSelection(mSharedPref.getInt(getString(R.string.region), 0));
		
		regions.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				eventsNull = false;
				Log.d("CalendarListFragment", "onItemSelected REGION");

				if (position != mSharedPref.getInt(getString(R.string.region), -1)) {
					if (MainActivity.DEBUG) {
						Log.d("CalendarListFragment", "onItemSelected REGION změna");
					}
					SharedPreferences.Editor editor = mSharedPref.edit();
					editor.putInt(getString(R.string.region), position);
					editor.commit();
					
					getEventsFromDatabase();
					orderList();
					displayList();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
			
		});
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
		builder.setView(layout);
		Dialog d = builder.create();
		d.setCanceledOnTouchOutside(true);
    	d.show();
    }
 
    /**
     * Zobrazí dialog pro seřazení seznamu a podle zvoleného nastavení seznam seřadí
     */
    private void orderByDialog() {
    	String name = mSharedPref.getString(getString(R.string.action_order_by), "");
    	int pos = -1;
    	if (name == "date") {
    		pos = 0;
    	} else if (name == "name") {
    		pos = 1;
    	} else if (name == "org1") {
    		pos = 2;
    	} else if (name == "region") {
    		pos = 3;
    	} else if (name == "sport") {
    		pos = 4;
    	} else if (name == "discipline") {
    		pos = 5;
    	} else if (name == "level") {
    		pos = 6;
    	} else {
    		pos = -1;
    	}
    	
    	AlertDialog.Builder popup = new AlertDialog.Builder(getActivity());
    	popup.setSingleChoiceItems(R.array.order_by_array, pos, new DialogInterface.OnClickListener()
        {
            @SuppressLint("CommitPrefEdits")
			public void onClick(DialogInterface dialog, int position)
            {
            	SharedPreferences.Editor editor = mSharedPref.edit();

            	switch(position) {
            		case 0:
            			editor.putString(getString(R.string.action_order_by), "date");
            			break;
            		case 1:
            			editor.putString(getString(R.string.action_order_by), "name");
            			break;
            		case 2:
            			editor.putString(getString(R.string.action_order_by), "org1");
            			break;
            		case 3:
            			editor.putString(getString(R.string.action_order_by), "region");
            			break;
            		case 4:
            			editor.putString(getString(R.string.action_order_by), "sport");
            			break;
            		case 5:
            			editor.putString(getString(R.string.action_order_by), "discipline");
            			break;
            		case 6:
            			editor.putString(getString(R.string.action_order_by), "level");
            			break;
            		default:
            			break;
            	}
            	editor.commit();
            	getEventsFromDatabase();
            	orderList();
            	displayList();
            	// zavři dialog
                dialog.dismiss();
            }
        });
    	
    	Dialog d = popup.create();
    	d.show();
    }

    /**
     * Zeřadí seznam sestupně/vzestupně podle nastavení z SharedPreferences
     */
    private void orderList() {
    	if (mSharedPref.getString(getString(R.string.action_order), null) == "sestupně") {
    		Collections.reverse(mCalendarEvents);
 	    }
    }
	
    /**
     * Zavře všechny podseznamy
     */
	public void resume() {
		int count = mExpListAdapter.getGroupCount();
        for (int i = 1; i <= count; i++) {
        	mExpListView.collapseGroup(i - 1);
        }
	}
	
    /**
	 * Vloží neměnná data (jako sporty, disciplíny a úrovně) do databáze.
	 */
    private void insertInitDataToDatabase() {
		this.helper.insertSport(1, "OB", "Foot O");
    	this.helper.insertSport(2, "LOB", "Ski O");
    	this.helper.insertSport(3, "MTBO", "MTB O");
    	this.helper.insertSport(4, "TRAIL", "Trail");
    	
    	this.helper.insertDiscipline(1, "KL", "Klasická trať", "Long distance");
    	this.helper.insertDiscipline(2, "KT", "Krátká trať", "Middle distrance");
    	this.helper.insertDiscipline(3, "SP", "Sprint", "Sprint");
    	this.helper.insertDiscipline(4, "DT", "Dlouhá trať", "Ultralong distance");
    	this.helper.insertDiscipline(5, "ST", "Štafety", "Relays");
    	this.helper.insertDiscipline(6, "DR", "Družstva", "Teams");
    	this.helper.insertDiscipline(7, "SC", "Volné pořadí kontrol", "Scorelauf");
    	this.helper.insertDiscipline(9, "NOB", "Noční", "Night");
    	this.helper.insertDiscipline(10, "Z", "Dlouhodobé výsledky", "Cups and ranking");
    	this.helper.insertDiscipline(11, "TeO", "TempO", "TempO");
    	
    	this.helper.insertLevel(1, "MČR", "Mistrovství ČR", "Czech champs");
    	this.helper.insertLevel(2, "ŽA", "Žebříček A", "A - level");
    	this.helper.insertLevel(3, "ŽB", "Žebříček B", "B - level");
    	this.helper.insertLevel(4, "OŽ", "Oblastní žebříček", "Local event");
    	this.helper.insertLevel(5, "V", "Vícedenní", "Multidays event");
    	this.helper.insertLevel(7, "ČPŠ", "Český pohár štafet", "Czech relays cup");
    	this.helper.insertLevel(8, "ČP", "Český pohár", "Czech cup");
    	this.helper.insertLevel(11, "OM", "Oblastní mistrovství", "Local champs");
    	this.helper.insertLevel(14, "OF", "Ostatní oficiální", "Other official");
    	
    	if (MainActivity.DEBUG) {
    		Log.d("CalendarListFragment", "insertInitData DONE");
    	}
    }	
    
    /**
     * Stažení závodů z API.
     */
    private void downloadEvents(){
    	try {
    		if (MainActivity.DEBUG)
    			Log.d("CalendarListFragment", "downloadEvents");
    		api.getEventList(OrisAPI.Sport.ALL_SPORTS,"","",dateFromStr,dateToStr,false);
    		if (MainActivity.DEBUG)
    			Log.d("CalendarListFragment", "downloadEvents done");
    	} catch (Exception e) {
    		if (refresh) {
    			getEventsFromDatabase();
    			displayList();
    		}
    		Toast.makeText(this.getActivity().getApplicationContext(), "Nemáš připojení k INTERNETu", Toast.LENGTH_SHORT).show();
    		e.printStackTrace();
    	}
    }
    
    /**
     * Načte závody z databáze.
     */
    private void getEventsFromDatabase() {
    	// Předání cursoru se záznamy z databáze
    	mCalendarEvents.clear();
    	
    	if (MainActivity.DEBUG) {
    		Log.d("CalendarListFragment", "getEventsFromDatabase, eventsNull = " + eventsNull);
    	}
    	
    	Cursor eventList = null;
    	int regionVal = mSharedPref.getInt(getString(R.string.region), 0);
    	int sportVal = mSharedPref.getInt(getString(R.string.sport), 0);

    	if (MainActivity.DEBUG) {
	    	Log.d("sportVal", String.valueOf(sportVal));
	    	Log.d("regionVal", String.valueOf(regionVal));
    	}
    	
    	String[] regs = getResources().getStringArray(R.array.regions_array);
    	
    	if (regionVal != 0) {
    		regs[regionVal] = regs[regionVal].substring(0, regs[regionVal].indexOf(" "));
    		eventList = this.helper.getEventsRegion(
    				dateFromStr, dateToStr, 
    				mSharedPref.getString(getString(R.string.action_order_by), "date"),
    				regs[regionVal],
    				sportVal);
    		
    	} else {
    		eventList = this.helper.getEvents(
    			dateFromStr, dateToStr, 
    			mSharedPref.getString(getString(R.string.action_order_by), "date"),
    			sportVal);
    	}
    	
    	if (MainActivity.DEBUG) {
    		Log.i("MainActivity", "eventList count: " + Integer.toString(eventList.getCount()));
    		Log.i("orderBy", mSharedPref.getString(getString(R.string.action_order_by), "date"));
    	}
		eventList.moveToFirst();
		
		
		if (eventList.getCount() != 0) {
			eventsNull = false;
			try {
				for (int i = 0; i < eventList.getCount(); i++) {
					Event newEvent = new Event(
							eventList.getInt(0),
							eventList.getString(1),
							eventList.getString(2),
							this.helper.getStringValue("club", "abbr", eventList.getInt(3)) 
							);
					
					// id, name, date, org1, region, sport, discipline, level
					newEvent.region = eventList.getString(4);
					newEvent.sport_str = this.helper.getStringValue("sport", "nameCZ", eventList.getInt(5));
					newEvent.discipline_str = this.helper.getStringValue("discipline", "shortName", eventList.getInt(6));
					newEvent.level_str = this.helper.getStringValue("level", "shortName", eventList.getInt(7));
					newEvent.latitude = eventList.getDouble(8);
					newEvent.longitude = eventList.getDouble(9);
					
					mCalendarEvents.add(newEvent);
					eventList.moveToNext();
				}
				// zavreni cursoru
				eventList.close();
				
			} catch (Exception e) {
				Toast.makeText(this.getActivity().getApplicationContext(), "Něco se pokazilo :-(", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}
    	} else if (eventsNull) {
    		Toast.makeText(this.getActivity().getApplicationContext(), "Žádné nalezené závody", Toast.LENGTH_SHORT).show();	
    	} else {
    		eventsNull = true;
    		downloadEvents();
    	}
    }
    
    /**
     * Kontroluje existenci dat v databázi
     * @param data Kontrolovaná data
     * @param i Index
     */
    private void checkData(JSONObject data, int i) {
    	try {
	    	JSONArray id_events = data.getJSONObject("Data").names();
	    	// otestování existence ID pro sport, discipline a level
			int sport_id = data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Sport").getInt("ID");
			int discipline_id = data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Discipline").getInt("ID");
			int level_id = data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Level").getInt("ID");
			// pokud sport s daným ID neexistuje, ulož ho do databáze
			if (!this.helper.recordExists("sport", sport_id)) {
				Log.i("MainActivity", "new sport ID");
				this.helper.insertSport(
						sport_id,
						data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Sport").getString("NameCZ"),
						data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Sport").getString("NameEN")
						);
			}
	
			if (!this.helper.recordExists("discipline", discipline_id)) {
				Log.i("MainActivity", "new discipline ID");
				this.helper.insertDiscipline(
						discipline_id,
						data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Discipline").getString("ShortName"),
						data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Discipline").getString("NameCZ"),
						data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Discipline").getString("NameEN")
						);
			}
	
			if (!this.helper.recordExists("level", level_id)) {
				Log.i("MainActivity", "new level ID");
				this.helper.insertLevel(
						level_id,
						data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Level").getString("ShortName"),
						data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Level").getString("NameCZ"),
						data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Level").getString("NameEN")
						);
			}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }

    /** Metoda z OrisAPIListener, ktera se asynchrone vola z OrisAPI s vysledkem.
     * @param data Data z weboveho API jako JSONObject
     * @param method Metoda o ktery dotaz se jedna OrisAPI.Method
    */
	@Override
	public void onDownloadComplete(JSONObject data, Method method) {

		switch(method){
			case GET_CSOS_CLUB_LIST:
				Log.d("CalendarListFragment", "GET_CSOS_CLUB_LIST");

				/** Vložení všech klubů do databáze */
				try {
					JSONArray clubs = data.getJSONObject("Data").names();
					int id = 0;
					String abbr = null;
					String name = null;
					
					if (MainActivity.DEBUG) {
						Log.d("CalendarListFragment", "Clubs number = " + String.valueOf(clubs.length()));
					}
					
					for (int i = 0; i < clubs.length(); i++) {
						id = data.getJSONObject("Data").getJSONObject(clubs.getString(i)).getInt("ID");
						name = data.getJSONObject("Data").getJSONObject(clubs.getString(i)).getString("Name");
						abbr = data.getJSONObject("Data").getJSONObject(clubs.getString(i)).getString("Abbr");
						this.helper.insertClub(id, abbr, name);
					}
					
					if (MainActivity.DEBUG) {
						Log.d("CalendarListFragment", "Clubs number after = " + String.valueOf(this.helper.tableCount("club")));
					}
					
				} catch (Exception e) {			
					Toast.makeText(this.getActivity().getApplicationContext(), "Něco se pokazilo :-(", Toast.LENGTH_SHORT).show();
					e.printStackTrace();
				}

			break;
			
			case GET_EVENT_LIST:
				try {
					if(data.has("Data")  && (data.optJSONObject("Data") != null) ){
						JSONArray id_events = data.getJSONObject("Data").names();
						Log.i("GET_EVENT_LIST", Integer.toString(id_events.length()));
						
						// Přes všechna vrácená data ulož každý závod do databáze
						for(int i = 0; i < id_events.length(); i++){
							int event_id = data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getInt("ID");
							
							checkData(data, i);
							
							int org2 = 0;
							if ((data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getString("Org2").compareTo("[]")) != 0) {
								org2 = data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Org2").getInt("ID");
							}
	
							// Pokud závod v databázi neexistuje, vloží se nový záznam
							if (!this.helper.recordExists("events", event_id)) {
								this.helper.insertEvent(
										event_id,
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getString("Name"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getString("Date"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Org1").getInt("ID"),
										org2,
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getString("Region"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Sport").getInt("ID"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Discipline").getInt("ID"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Level").getInt("ID"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getString("EntryDate1"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getString("EntryDate2"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getString("EntryDate3"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getInt("Ranking"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getInt("Cancelled"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getDouble("GPSLat"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getDouble("GPSLon") );
							} else { // Pokud závod v databázi exituje, updatují se informace v databázi
								this.helper.updateEvent(
										event_id,
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getString("Name"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getString("Date"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Org1").getInt("ID"),
										org2,
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getString("Region"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Sport").getInt("ID"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Discipline").getInt("ID"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getJSONObject("Level").getInt("ID"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getString("EntryDate1"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getString("EntryDate2"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getString("EntryDate3"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getInt("Ranking"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getInt("Cancelled"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getDouble("GPSLat"),
										data.getJSONObject("Data").getJSONObject(id_events.getString(i)).getDouble("GPSLon") );							
								}
						}
					}
				
				} catch (Exception e) {			
					//Toast.makeText(this.getActivity().getApplicationContext(), "Něco se pokazilo :-(", Toast.LENGTH_SHORT).show();
					e.printStackTrace();
				}
				
				getEventsFromDatabase();	        	
	        	displayList();
				
			break;
			
			default:
			break;
				
			}
	} 	
	
	@Override
	public void onDestroyView() {
		if (MainActivity.DEBUG) {
			Log.i("CalendarListFragment", "onDestroyView");
		}
		/** Ukonceni ukazatele na databázi */
		helper.close();
		super.onDestroyView();		
	}
	
	// TODO ??? NÁPAD NA VYLEPŠENÍ - přidat tlačítko do ActionBaru "Stáhnout všechny závody" -> upozornění na velké stahování dat a že to bude nějakou dobu trvat (dialog ANO/NE) ->
    // stáhnout celý kalendář do databáze i s detaily závodů???
	
}
